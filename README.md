# Beegfs Evaluation

This repository contains the scrips, log files, and results of the paper entitled _"The role of storage target 
allocation in applications' I/O performance with BeeGFS "_.

*You can find the submitted version of our paper in `submitted.pdf`*

This repository is organized according to the evaluated scenario.

- `scenario_01`: results related to the first evaluated scenario (the network limits I/O performance)
- `scenario_02`: results of the second evaluated scenario (the storage devices limit I/O performance)

We generated all practical tests using IOR version 3.4 compiled with GCC v.4.8.5. IOR was download
from https://github.com/hpc/ior and for our tests we used the commit 8475c7d30025dd5e39147c251bf84e1ed24b9858

All tests present in our manuscripts can be executed using the scripts 
`scenario_01/launcher_all.sh` and `scenario_02/launcher_all.sh`. 

The script launches the jobs described in the `slurm_start.sh` files available in the sub-folders.

Each `slurm_start.sh` file describes one test case using IOR. 

:warning: Depending on the platform, those files maybe need to be adapted.


You can also reproduce all graphs from our manuscript as follows:


First, install the dependencies

```bash
pip install -r requirement.txt 
```

Then, generate the graphs using the available scripts

```bash
cd plot
python3 figure_02.py # Figures 2a and 2b
python3 figure_04.py # Figures 4a and 4b
python3 figure_05.py # Figures 5a and 5b
python3 figure_06.py # Figures 6a and 6b
python3 figure_08.py # Figure 8
python3 figure_10.py # Figure 10
python3 figure_11.py # Figure 11
python3 figure_12.py # Figures 12a, 12b and 12c
python3 figure_13.py # Figure 13
```