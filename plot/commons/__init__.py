"""
This package contains a series of functions to handle the CSV files generated from the IOR and tracker data

Functions are mainly related to reading, sanitize and compute data. The motivation to create this
file came from the fact that many of those functions are commonly used in different Jupiter notebooks,
so I decided to make a code refactoring to clean the notebooks by moving those functions to the same file.

A positive side effect of that is the minimization of code errors.  I will standardize the reading
and the loading procedures in the same functions, and I will not write it again and again whenever
I start a new notebook.
"""