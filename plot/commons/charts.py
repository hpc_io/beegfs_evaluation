import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def boxplot_v1(xlabels, values, title=None, xlabel=None, ylabel=None, xrotation=None,
               plot_weight=11, plot_height=7, xlim=None, ylim=None, save_to=None):
    """
       This function plots a boxplot graph adding the plotol of the model  rate: 1100 * (1 + rate)
       The number of box plots is defined according to the size of the list of values passed as input.
       :param xlabels: List[str] with all labels of the x-axis
       :param values: List[numpy.array] with with the values to generate the box plots. len(xlabels) == len(values)
       :param title:  The title of the graph
       :param xlabel: The label of the the x-axis
       :param ylabel: The label of the the y-axis
       :param xrotation: The rotation degree of the xlabels
       :param plot_weight: the weight of the graph in inches
       :param plot_height: the height of the graph in inches
       :param xlim: List[float, float] the limits of the x-axis
       :param ylim: List[float, float] the limits of the y-axis
       :param save_to: Full path to a pdf file where the graph will be saved
       """

    fig, ax = plt.subplots()

    fig.set_size_inches(plot_weight, plot_height, forward=True)

    ax.boxplot(values)

    plt.title(title, fontweight='bold', fontsize=18)

    plt.xlim(xlim)
    ax.set_xticklabels(xlabels)
    plt.xticks(rotation=xrotation)
    plt.xlabel(xlabel, fontweight='bold', fontsize=18)

    plt.ylim(ylim)
    plt.ylabel(ylabel, fontsize=18, fontweight='bold')

    ax.tick_params(axis='both', which='major', labelsize=14)

    bw = lambda rate: 1100 * (1 + rate)
    ax.plot([i for i in range(1, 13)], [bw(v[0] / v[1]) for v in xlabels], label='$1100 x (1 + min/max)$')

    # The code below added the number of occurrences in each box plotol (kept in the code just to history proposes)
    # n_obs = [len(d['bw'].values) for d in dframes]
    # for i in range(len(n_obs)):
    #     ax.text(i + 1, dframes[i]['bw'].max() + 30, f'{n_obs[i]}',fontsize=10,
    #     horizontalalignment='center', color='blue', fontweight='bold')

    if save_to is not None:
        fig.savefig(save_to, format='pdf', dpi=1200)

    plt.legend()
    plt.grid()
    plt.show()


def boxplot_v2(xlabels, values, title=None, xlabel=None, ylabel=None, xrotation=None,
               plot_weight=11, plot_height=7, xtick_fontsize=18, x_fontsize=20, y_fontsize=20, xlim=None, ylim=None, format="pdf", save_to=None):
    '''
    This function plots a boxplot graph.
    The number of box plots is defined according to the size of the list of values passed as input.
    :param xlabels: List[str] with all labels of the x-axis
    :param values: List[numpy.array] with with the values to generate the box plots. len(xlabels) == len(values)
    :param title:  The title of the graph
    :param xlabel: The label of the the x-axis
    :param ylabel: The label of the the y-axis
    :param xrotation: The rotation degree of the xlabels
    :param plot_weight: the weight of the graph in inches
    :param plot_height: the height of the graph in inches
    :param xlim: List[float, float] the limits of the x-axis
    :param ylim: List[float, float] the limits of the y-axis
    :param save_to: Full path to a pdf file where the graph will be saved
    '''
    # start plotol env
    fig, ax = plt.subplots()
    # Define plotol size
    fig.set_size_inches(plot_weight, plot_height, forward=True)

    ax.boxplot(values)

    plt.title(title, fontweight='bold', fontsize=22)

    plt.xlim(xlim)
    ax.set_xticklabels(xlabels)
    plt.xticks(rotation=xrotation)
    plt.xlabel(xlabel, fontsize=x_fontsize, fontweight='bold')

    plt.ylim(ylim)
    plt.ylabel(ylabel, fontsize=y_fontsize, fontweight='bold')

    ax.tick_params(axis='both', which='major', labelsize=xtick_fontsize)

    plt.grid()
    # plt.legend()

    if save_to:
        fig.savefig(save_to, format=format, dpi=1200)

    #plt.show()


def scatter_v1(x: np.ndarray, y: np.ndarray, x_mean=None, y_mean=None, title=None, xlabel=None, ylabel=None,
               plot_weight=11, plot_height=7,
               xlim=None, ylim=None, format="pdf",
               save_to=None, color=None, alpha=None, grid=False):
    """
    plotol a scatter graph considering only one type of data
    :param x: x-values
    :param y: y-values
    :param title:  The title of the graph
    :param xlabel: The label of the the x-axis
    :param ylabel: The label of the the y-axis
    :param plot_weight: the weight of the graph in inches
    :param plot_height: the height of the graph in inches
    :param xlim: List[float, float] the limits of the x-axis
    :param ylim: List[float, float] the limits of the y-axis
    :param save_to: Full path to a pdf file where the graph will be saved
    :param color: color of the dots
    :param alpha: controls the intensity of the color
    :param grid: if true it shows grid lines in the graph
    :return:
    """
    fig, ax = plt.subplots()

    fig.set_size_inches(plot_weight, plot_height, forward=True)

    if x_mean is not None and y_mean is not None:
        plt.plot(x_mean, y_mean, '--', color='red')

    plt.scatter(x, y, alpha=alpha, color=color)

    plt.title(title, fontweight='bold', fontsize=22)

    ax.set_xlim(xlim)
    plt.xlabel(xlabel, fontweight='bold', fontsize=22)

    ax.set_ylim(ylim)
    plt.ylabel(ylabel, fontsize=22, fontweight='bold')

    ax.tick_params(axis='both', which='major', labelsize=16)

    if grid:
        plt.grid()

    if save_to:
        fig.savefig(save_to, format=format, dpi=1200)


def scatter_v2(x, y, colors, alphas, labels,
               title=None, xlabel=None, ylabel=None, plot_weight=11, plot_height=7,
               xlim=None, ylim=None,
               save_to=None,
               grid=False):
    """
    plotol a scatter graph considering different type of data. List of x and y values
    :param x:  x-values
    :param y: y-values
    :param title:  The title of the graph
    :param xlabel: The label of the the x-axis
    :param ylabel: The label of the the y-axis
    :param plot_weight: the weight of the graph in inches
    :param plot_height: the height of the graph in inches
    :param xlim: List[float, float] the limits of the x-axis
    :param ylim: List[float, float] the limits of the y-axis
    :param save_to: Full path to a pdf file where the graph will be saved
    :param colors: a list of colors for each dot type
    :param alphas: a list of values to controls the intensity of the different dot's colors
    :param labels: a list of labels to each dot type
    :param grid: if true it shows grid lines in the graph
    """
    fig, ax = plt.subplots()

    fig.set_size_inches(plot_weight, plot_height, forward=True)

    for i in range(len(x)):
        plt.scatter(x[i], y[i], alpha=alphas[i], color=colors[i], label=labels[i])

    plt.title(title, fontweight='bold', fontsize=22)

    ax.set_xlim(xlim)
    plt.xlabel(xlabel, fontweight='bold', fontsize=22)

    ax.set_ylim(ylim)
    plt.ylabel(ylabel, fontsize=22, fontweight='bold')

    ax.tick_params(axis='both', which='major', labelsize=16)

    if grid:
        plt.grid()

    if save_to:
        fig.savefig(save_to, format='pdf', dpi=1200)

    plt.legend()
    plt.show()


def barchart_v1(x_labels, y, error, colours,
                title=None, ylabel=None, ylim=None, xlabel=None, plot_weight=11, plot_height=7):
    """
    Shows a default barchart
    :param x_labels: list of labels will be put on x-axis
    :param y: barchart value
    :param colours:  Colours of Bars
    :param title:  Title
    :param ylabel: y-xis labels
    :param xlabel: x-xis labels
      :param plot_weight: the weight of the graph in inches
    :param plot_height: the height of the graph in inches
    :param save_to: Full path to a pdf file where the graph will be saved
    """
    # First, create the x_pos vector in the labels
    x_pos = [i for i, _ in enumerate(x_labels)]

    fig, ax = plt.subplots()

    fig.set_size_inches(plot_weight, plot_height, forward=True)

    ax.bar(x_pos, y, yerr=error, align='center', alpha=0.5, ecolor='black', capsize=10)

    # Define BAR colours
    for i in range(1, len(colours) + 1):
        ax.get_children()[i].set_color(colours[i - 1])

    # Define Labels
    plt.ylabel(ylabel, fontsize=18, fontweight='bold')
    plt.title(title, fontsize=18, fontweight='bold')
    plt.xlabel(xlabel, fontsize=14, fontweight='bold')
    plt.xticks(x_pos, x_labels)
    ax.tick_params(axis='both', which='major', labelsize=14)
    plt.ylim(ylim)

    plt.show()


def plot(x, y, xlabel=None, ylabel=None, title=None, ylim=None, xlim=None):
    """
    plotol a line graph
    :param x: list of x values
    :param y: list of y values
    :param xlabel: x-axis labels
    :param ylabel: y-axis labels
    :param title: Title
    :param ylim: y-axis limits
    :param xlim: x-axis limits
    """
    fig, ax = plt.subplots()

    # Plot
    ax.plot(x, y, '--bo')

    # Define Labels
    plt.xticks(np.arange(0, max(x) + 1, 1))
    plt.xlabel(xlabel, fontsize=16)
    plt.ylabel(ylabel, fontsize=16, labelpad=20)
    plt.title(title)

    plt.xlim(xlim)
    plt.ylim(ylim)
    plt.grid()

    plt.show()


def bw_single_histogram(df: pd.DataFrame, title=None, xlabel='Bandwidth (MiB/s)', ylabel='Number of Occurrences',
                        xlim=None, ylim=None, xticks=None, save_to=None, height=8, weight=7, bins=200):
    """
    Shows a default histogram with all bandwidth of a single application
    :param df: a pandas dataframe with an single application
    :param title: Histogram Title
    :param xlabel:  Label of x-axis
    :param ylabel: Label of Y-axis
    :param xlim: Limits of x-axis
    :param ylim: Limits of y-axis
    :param xticks: fixed ticks to be used on x-axis (should be generated with 'np.arange(a, b, step)'
    :param bins: Number of histogram bins
    """

    fig, ax = plt.subplots()

    fig.set_size_inches(weight, height, forward=True)

    _ = ax.hist(df['bw'].values, bins, alpha=0.5, histtype='bar', ec='black')

    plt.ylabel(ylabel, fontsize=14, fontweight='bold')
    plt.xlabel(xlabel, fontsize=14, fontweight='bold')
    plt.title(title, fontsize=18, fontweight='bold')

    ax.tick_params(axis='both', which='major', labelsize=14)

    plt.ylim(ylim)

    if save_to:
        fig.savefig(save_to, format='pdf', dpi=1200)

    plt.show()



def bw_plot_scatter(x_values, y_values, x_mean, y_mean, xlabel=None, ylabel=None, title=None, xlim=None, ylim=None, save_to=None, format='pdf'):
    fig, ax = plt.subplots()

    fig.set_size_inches(11, 8, forward=True)

    plt.scatter(x_values, y_values, alpha=.6, color='lightblue')
    plt.plot(x_mean, y_mean, 'o--', alpha=0.6, color="blue")

    # Define Labelsabs
    plt.ylim(ylim)
    plt.xlim(xlim)

    plt.ylabel(ylabel, fontsize=22, fontweight='bold')
    plt.xlabel(xlabel, fontweight='bold', fontsize=18)

    plt.title(title, fontsize=22, fontweight="bold")

    ax.tick_params(axis='both', which='major', labelsize=16)
    # ax.tick_params(axis='both', which='minor', labelsize=8)
    ax.set_xticks(x_mean)
    plt.grid()

    if save_to:
        fig.savefig(save_to, format=format, dpi=1200)
    # plt.show()


# PLot Functions
def bw_aggr_barchart(df: pd.DataFrame):
    """
    Shows a default barchart with the average bandwidth of AppA, AppB and the aggregated BW
    :param df: a pandas dataframe with both application per row, and the computed aggregated_bandwidth
    """

    x = ['App-A', 'App-B', "Aggregated"]
    x_pos = [i for i, _ in enumerate(x)]
    y_mean = [df['our-bw_a'].mean(), df['our-bw_b'].mean(), df['bw_aggr'].mean()]
    error = [df['our-bw_a'].std(), df['our-bw_b'].std(), df['bw_aggr'].std()]

    fig, ax = plt.subplots()

    ax.bar(x_pos, y_mean, yerr=error, align='center', alpha=0.5, ecolor='black', capsize=10)

    # Define Labels
    plt.ylabel('Mean Bandwidth(MiB/s)')

    plt.xticks(x_pos, x)
    # plt.grid()

    plt.show()


def bw_aggr_histogram(df: pd.DataFrame, title, xlabel='Bandwidth (MiB/s)', ylabel='Number of Occurrences',
                      xlim=None, ylim=None, bins=20):
    """
    Shows a default histogram with  bandwidth distributions of application A, B and the Aggregate bandwidth
    :param df: a pandas dataframe with both applications per row, and the computed aggregated_bandwidth
    :param title: Histogram Title
    :param xlabel:  Label of x-axis
    :param ylabel: Label of Y-axis
    :param xlim: Limits of x-axis
    :param ylim: Limits of y-axis
    :param bins: Number of histogram bins
    """
    plt.hist(df['our-bw_a'].values, bins, alpha=0.5, label='app A', histtype='bar', ec='black')
    plt.hist(df['our-bw_b'].values, bins, alpha=0.5, label='app B', histtype='bar', ec='black')
    plt.hist(df['bw_aggr'].values, bins, alpha=0.5, label='aggregated', histtype='bar', ec='black')

    plt.title(title, size=16)

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.xlim(xlim)
    plt.ylim(ylim)

    plt.legend(loc='upper left', prop={'size': 9})
    plt.show()


def bw_barchart(singles, aggrs, bar_labels, colours,
                title=None, ylabel=None, xlabel=None, xticks_rotation=None, save_to=None):
    """
    Shows a default barchart with the average bandwidth of single and aggregated executions,
    :param singles: list of pandas Dataframe with single app executions
    :param aggrs: list of pandas Dataframe with aggregate execution
    :param bar_labels: Labels of bars
    :param colours:  Colours of Bars
    :param title:  Title
    :param ylabel: y-xis labels
    :param xlabel: x-xis labels
    """
    # First, create the x_pos vector in the labels
    x_pos = [i for i, _ in enumerate(bar_labels)]

    # Next, compute the y_mean, and the error values
    y_mean = []
    error = []

    # compute it for the single applications
    for single in singles:
        y_mean.append(single['bw'].mean())
        error.append(single['bw'].std())

    # compute it for the aggregation executions
    for aggr in aggrs:
        y_mean.extend([aggr['our-bw_a'].mean(),
                       aggr['our-bw_b'].mean(),
                       aggr['bw_aggr'].mean()])

        error.extend([aggr['our-bw_a'].std(),
                      aggr['our-bw_b'].std(),
                      aggr['bw_aggr'].std()])

    fig, ax = plt.subplots()

    fig.set_size_inches(11, 7, forward=True)
    ax.bar(x_pos, y_mean, yerr=error, align='center', alpha=0.5, ecolor='black', capsize=10)

    # Define BAR colours
    for i in range(1, len(colours) + 1):
        ax.get_children()[i].set_color(colours[i - 1])

    plt.xticks(rotation=xticks_rotation)
    # Define Labels
    plt.ylabel(ylabel, fontsize=18, fontweight='bold')
    plt.title(title, fontsize=18, fontweight='bold')
    plt.xlabel(xlabel, fontsize=14, fontweight='bold')
    plt.xticks(x_pos, bar_labels)
    ax.tick_params(axis='both', which='major', labelsize=14)

    if save_to:
        fig.savefig(save_to, format='pdf', dpi=1200)

    plt.show()
