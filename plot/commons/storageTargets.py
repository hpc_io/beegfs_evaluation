import pandas
import pandas as pd

'''
That package includes some useful functions to handle the storage targets data (manly data from tracker.sh file)
'''


def __compute_balance_rate(targets):
    """
    This function receives a list of targets and computes the server balance rate: (min, max)
    Note that this function considered just two servers.
    :param targets: a list of the used targets (int or str)
    :return: (min, max) info
    """

    s1 = 0
    s2 = 0
    for target in targets:
        if int(target) < 200:
            s1 += 1
        else:
            s2 += 1

    return min(s1, s2), max(s1, s2)


def storageDataframe(df_ior, df_tracker):
    """
    This function receives an IOR CSV's dataframe and its tracker dataframe
    and returns a new dataframe (storage dataframe) with the following information:
    bw, start:datatime, end:datatime, targets: list, timestamp (in seconds), count: targets balance rate..

    :param df_ior: a pandas.Dataframe loaded with an IOR CSV
    :param df_tracker: a pandas.Dataframe loaded with a tracker CSV
    :return: a pandas.Dataframe with the targets information and the bandwidth
    """

    # dfa = csv.load_ior(csv_f)
    # dft = csv.load_tracker(tracker_f)

    df = pd.DataFrame()

    df['bw'] = df_ior['bw']
    df['start'] = df_ior['start']
    df['end'] = df_ior['end']
    df['targets'] = df_tracker['targets']

    #t0 = df['end'][0]
    #df['timestamp'] = df['end'].apply(lambda x: (x - t0).seconds)

    df['balance_rate'] = df['targets'].apply(lambda x: __compute_balance_rate(x))

    # gb = df.groupby('balance_rate').agg({'bw': ['count', 'mean', 'min', 'max']})
    # gb.columns = ['occurrences', 'bw_mean', 'bw_min', 'bw_max']
    # gb = gb.reset_index()

    return df


def split_by_balance_rate(df):
    """
    This function receives a storageDataframe and split into different frames by its balance rates.
    :param df: a storageDataframe
    :return: a list of storageDataframes
    """

    # get the number of targets
    n_max = len(df['targets'][0])
    n_min = 0

    dataframes = []
    # check all possibilities of balance_rate
    while n_min <= n_max:
        cframe = df.loc[(df['balance_rate'] == (n_min, n_max))]

        # if dataframe is not empty, put it on the list
        if not cframe.empty:
            dataframes.append(cframe)

        # update the balance rates
        n_max -= 1
        n_min += 1

    return dataframes


def compute_aggregate_bw(df_a, df_b, sigma, n_tasks):
    """
    That function computes the aggregate bandwidth of two applications.
    Firstly, we compute the bandwidth of df_a and df_b not taking in account the latency.
    Then, we compute the aggregated bandwidth
    :param df_a: a pandas dataframe with loaded with the ior csv of application 'A'
    :param df_b: a pandas dataframe with loaded with the ior csv of application 'B'
    :param sigma: the time delay used in the test
    :return df: a pandas dataframe with both application per row, and the computed aggregated_bandwidth
    """

    # Firstly, we create the DataFrame df
    df = pd.DataFrame()

    # Compute the volume in MiB
    df['volume_a'] = (df_a['block'] / 1024) * n_tasks
    df['volume_b'] = (df_b['block'] / 1024) * n_tasks
    # compute the bandwidth for appA
    df['bw_a'] = df_a['bw']
    # df['our-bw_a'] = round(df['volume_a'] / df_a['total'], 2)
    # compute the bandwidth for appB
    df['bw_b'] = df_b['bw']
    # df['our-bw_b'] = round(df['volume_b'] / df_b['total'], 2)

    # Computing  max(total_b + sigma, total_a)
    aux = pd.DataFrame()
    aux['total_a'] = df_a['total']
    aux['total_b'] = df_b['total'] + sigma  # total_b + sigma
    df['total_max'] = aux[['total_a', 'total_b']].max(axis=1)

    # compute aggregated bandwidth
    # (Vol_a+Vol_b)/(max(sigma+total_b, total_a))
    df['bw_aggr'] = (df['volume_a'] + df['volume_b']) / df['total_max']

    return df


def compute_aggregate_bw_v2(df_dict, volume_total):
    """
    That function computes the aggregate bandwidth of parallel applications.
    :param volume_total: volume_total: the total data volume transfer by all applications(MB)
    :param df_dict: a list with pandas dataframes loaded with the ior csv of each parallel application
    :return df
    """

    # Firstly, we create the DataFrame df
    df_all = pd.DataFrame()

    for app_id, df in df_dict.items():
        # get the bandwidths
        df_all[f'bw_{app_id}'] = df['bw']

    columns = []
    aux = pd.DataFrame()
    for app_id, df in df_dict.items():
        # Computing  max(total_b + sigma, total_a)
        column = f'total_{app_id}'
        aux[column] = df['total']
        columns.append(column)

    df_all['total_max'] = aux[columns].max(axis=1)

    # compute aggregated bandwidth
    # (Vol_a+Vol_b)/(max(sigma+total_b, total_a))
    df_all['bw_aggr'] = (volume_total) / df_all['total_max']

    return df_all

# def check_data(dfa, dfb, sigma):
#     aux = pd.DataFrame()
#
#     aux['end_b'] = dfb['end']
#     aux['end_check'] = dfa['start'] + dfb['total'].apply(lambda x: timedelta(seconds=x)) + timedelta(seconds=sigma)
#
#     aux['diff'] = aux['end_check'] - aux['end_b']
#
#     aux['diff'] = aux['diff'].apply(lambda x: abs(x.total_seconds()))
#
#     print(f"Mean: {aux['diff'].mean():0.2f} +/-{aux['diff'].std(): 0.2f}")
