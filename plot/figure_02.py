from commons import csv, charts
import matplotlib.pyplot as plt

path_sc1 = "../scenario_01"
path_sc2 = "../scenario_02"


# generating Figure 02-a

format="png"

def plot_figure(x_labels, y_values, y_max, y_min, save_to):
    fig, ax = plt.subplots()

    fig.set_size_inches(11, 9.5, forward=True)
    ax.plot(x_labels, y_values, '--', label='Mean')
    ax.fill_between(x_labels, y_max, y_min, color='blue', alpha=0.1, label='Max/min interval')

    # Define Labels
    plt.xticks(rotation=30)
    plt.xlabel('File Size (GB)', fontweight='bold', fontsize=22)
    plt.ylabel('Bandwidth (MiB/s)', fontsize=22, fontweight='bold')
    # plt.title('Bandwidth evolution according to the transferred aggregated file size')
    # plt.ylim(ylim)

    ax.tick_params(axis='both', which='major', labelsize=16)
    

    # plt.legend()
    plt.grid()

    fig.savefig(save_to, format=format, dpi=1200)
    # plt.show()


# generating Fig.2A
l2 = csv.load_all_ior([path_sc1 + "/file_size/01/csv/exec_01/output.csv",
                       path_sc1 + "/file_size/02/csv/exec_01/output.csv",
                       path_sc1 + "/file_size/03/csv/exec_01/output.csv",
                       path_sc1 + "/file_size/04/csv/exec_01/output.csv",
                       path_sc1 + "/file_size/05/csv/exec_01/output.csv",
                       path_sc1 + "/file_size/06/csv/exec_01/output.csv",
                       path_sc1 + "/file_size/07/csv/exec_01/output.csv",
                       path_sc1 + "/file_size/08/csv/exec_01/output.csv",
                       path_sc1 + "/file_size/09/csv/exec_01/output.csv",
                       path_sc1 + "/file_size/10/csv/exec_01/output.csv",
                       path_sc1 + "/file_size/11/csv/exec_01/output.csv",
                       path_sc1 + "/file_size/12/csv/exec_01/output.csv",
                       path_sc1 + "/file_size/13/csv/exec_01/output.csv"])

x_labels = []
y_values = []
y_max = []
y_min = []
for i in range(len(l2)):
    x_labels.append(f"{(l2[i].block[0] * 32) / 2 ** 20}")
    y_values.append(l2[i].bw.mean())
    y_max.append(l2[i].bw.max())
    y_min.append(l2[i].bw.min())

plot_figure(x_labels=x_labels, y_values=y_values, y_max=y_max, y_min=y_min, save_to=f'fig2a.{format}')

# Generating Fig. 2B
l1 = csv.load_all_ior([path_sc2 + "/file_size/12/csv/exec_01/output.csv",
                       path_sc2 + "/file_size/13/csv/exec_01/output.csv",
                       path_sc2 + "/file_size/01/csv/exec_01/output.csv",
                       path_sc2 + "/file_size/02/csv/exec_01/output.csv",
                       path_sc2 + "/file_size/03/csv/exec_01/output.csv",
                       path_sc2 + "/file_size/04/csv/exec_01/output.csv",
                       path_sc2 + "/file_size/05/csv/exec_01/output.csv",
                       path_sc2 + "/file_size/06/csv/exec_01/output.csv",
                       path_sc2 + "/file_size/07/csv/exec_01/output.csv",
                       path_sc2 + "/file_size/08/csv/exec_01/output.csv",
                       path_sc2 + "/file_size/09/csv/exec_01/output.csv",
                       path_sc2 + "/file_size/10/csv/exec_01/output.csv",
                       path_sc2 + "/file_size/11/csv/exec_01/output.csv"])

x_labels = []
y_values = []
y_max = []
y_min = []
for i in range(len(l1)):
    x_labels.append(f"{(l1[i].block[0] * 32) / 2 ** 20}")
    y_values.append(l1[i].bw.mean())
    y_max.append(l1[i].bw.max())
    y_min.append(l1[i].bw.min())

plot_figure(x_labels=x_labels, y_values=y_values, y_max=y_max, y_min=y_min, save_to=f'fig2b.{format}')
