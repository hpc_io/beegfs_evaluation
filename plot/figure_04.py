from commons import csv, charts
import matplotlib.pyplot as plt
from numpy import array

path_sc1 = "../scenario_01"
path_sc2 = "../scenario_02"


format = "png"

def generate_xy_values(x_array, csv_list, max_lines=100):
    l = csv.load_all_ior(csv_list)

    i = 0
    for df in l:
        df.insert(0, 'n_nodes', x_array[i])
        l[i] = l[i].head(max_lines)
        i += 1

    x_values = [df.n_nodes for df in l]
    y_values = [df.bw for df in l]
    y_mean = [df.bw.mean() for df in l]

    return x_values, y_values, y_mean, l


x4 = array([1, 2, 4, 8, 16])

# generating Fig 4A
x4_values, y4_values, y4_mean, l4 = generate_xy_values(x4, [
    path_sc1 + '/compute_nodes/4_targets/8_process/01/csv/exec_01/output.csv',
    path_sc1 + '/compute_nodes/4_targets/8_process/02/csv/exec_01/output.csv',
    path_sc1 + '/compute_nodes/4_targets/8_process/04/csv/exec_01/output.csv',
    path_sc1 + '/compute_nodes/4_targets/8_process/08/csv/exec_01/output.csv',
    path_sc1 + '/compute_nodes/4_targets/8_process/16/csv/exec_01/output.csv'],
                                                       max_lines=100)

charts.bw_plot_scatter(x4_values, y4_values, x4, y4_mean, xlabel='Number of Compute Nodes', ylabel='Bandwidth (MiB/s)', format=format, save_to=f"fig4a.{format}")

x = array([1, 2, 4, 8, 16, 32])
# generating Fig 4B
x4_values, y4_values, y4_mean, l4 = generate_xy_values(x, [
    path_sc2 + "/compute_nodes/4_targets/8_process/01/csv/exec_01/output.csv",
    path_sc2 + "/compute_nodes/4_targets/8_process/02/csv/exec_01/output.csv",
    path_sc2 + "/compute_nodes/4_targets/8_process/04/csv/exec_01/output.csv",
    path_sc2 + "/compute_nodes/4_targets/8_process/08/csv/exec_01/output.csv",
    path_sc2 + "/compute_nodes/4_targets/8_process/16/csv/exec_01/output.csv",
    path_sc2 + "/compute_nodes/4_targets/8_process/32/csv/exec_02/output.csv"],
                                                       max_lines=100)

charts.bw_plot_scatter(x4_values, y4_values, x, y4_mean, xlabel='Number of Compute Nodes', ylabel='Bandwidth (MiB/s)', format=format, save_to=f"fig4b.{format}") 
