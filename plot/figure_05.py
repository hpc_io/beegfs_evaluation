from commons import csv, charts
import matplotlib.pyplot as plt
from numpy import array

path_sc1 = "../scenario_01"
path_sc2 = "../scenario_02"

format = 'png'

def generate_xy_values(x_array, csv_list, max_lines=100):
    l = csv.load_all_ior(csv_list)

    i = 0
    for df in l:
        df.insert(0, 'n_nodes', x_array[i])
        l[i] = l[i].head(max_lines)
        i += 1

    x_values = [df.n_nodes for df in l]
    y_values = [df.bw for df in l]
    y_mean = [df.bw.mean() for df in l]

    return x_values, y_values, y_mean, l


def plot_graph(x, y4, y16, x4_values, y4_values, x16_values, y16_values, save_to):
    fig, ax = plt.subplots()

    fig.set_size_inches(11, 7, forward=True)

    plt.scatter(x4_values, y4_values, alpha=.6, color='lightblue')
    plt.plot(x, y4, 'v--', alpha=0.6, color="blue", label='8 processes per node')

    plt.scatter(x16_values, y16_values, alpha=.6, color='lightcoral')
    plt.plot(x, y16, 'o--', alpha=0.6, color="red", label='16 processes per node')

    plt.ylabel('Bandwidth MiB/s', fontsize=22, fontweight='bold')
    plt.xlabel('Number of Compute Nodes', fontweight='bold', fontsize=22)

    ax.tick_params(axis='both', which='major', labelsize=16)

    plt.legend(prop={'size': 14})
    plt.grid()
    fig.savefig(save_to, format=format, dpi=1200)


# generating Fig5A
x = array([1, 2, 4, 8, 16])

# generating Fig 4A
x4_values, y4_values, y4, l4 = generate_xy_values(x, [
    path_sc1 + '/compute_nodes/4_targets/8_process/01/csv/exec_01/output.csv',
    path_sc1 + '/compute_nodes/4_targets/8_process/02/csv/exec_01/output.csv',
    path_sc1 + '/compute_nodes/4_targets/8_process/04/csv/exec_01/output.csv',
    path_sc1 + '/compute_nodes/4_targets/8_process/08/csv/exec_01/output.csv',
    path_sc1 + '/compute_nodes/4_targets/8_process/16/csv/exec_01/output.csv'],
                                                  max_lines=100)

x16_values, y16_values, y16, l16 = generate_xy_values(x, [
    path_sc1 + '/compute_nodes/4_targets/16_process/01/csv/exec_01/output.csv',
    path_sc1 + '/compute_nodes/4_targets/16_process/02/csv/exec_01/output.csv',
    path_sc1 + '/compute_nodes/4_targets/16_process/04/csv/exec_01/output.csv',
    path_sc1 + '/compute_nodes/4_targets/16_process/08/csv/exec_01/output.csv',
    path_sc1 + '/compute_nodes/4_targets/16_process/16/csv/exec_01/output.csv'],
                                                      max_lines=25)

plot_graph(x, y4, y16, x4_values, y4_values, x16_values, y16_values, save_to=f'fig_5a.{format}')

# generating Fig 5B


x = array([1, 2, 4, 8, 16, 32])

x4_values, y4_values, y4, l4 = generate_xy_values(x, [
    path_sc2 + "/compute_nodes/4_targets/8_process/01/csv/exec_01/output.csv",
    path_sc2 + "/compute_nodes/4_targets/8_process/02/csv/exec_01/output.csv",
    path_sc2 + "/compute_nodes/4_targets/8_process/04/csv/exec_01/output.csv",
    path_sc2 + "/compute_nodes/4_targets/8_process/08/csv/exec_01/output.csv",
    path_sc2 + "/compute_nodes/4_targets/8_process/16/csv/exec_01/output.csv",
    path_sc2 + "/compute_nodes/4_targets/8_process/32/csv/exec_02/output.csv"],
                                                  max_lines=100)

x16_values, y16_values, y16, l416 = generate_xy_values(x, [
    path_sc2 + '/compute_nodes/4_targets/16_process/01/csv/exec_01/output.csv',
    path_sc2 + '/compute_nodes/4_targets/16_process/02/csv/exec_01/output.csv',
    path_sc2 + '/compute_nodes/4_targets/16_process/04/csv/exec_01/output.csv',
    path_sc2 + '/compute_nodes/4_targets/16_process/08/csv/exec_01/output.csv',
    path_sc2 + '/compute_nodes/4_targets/16_process/16/csv/exec_01/output.csv',
    path_sc2 + '/compute_nodes/4_targets/16_process/32/csv/exec_01/output.csv'], max_lines=25)


plot_graph(x, y4, y16, x4_values, y4_values, x16_values, y16_values, save_to=f'fig_5b.{format}')