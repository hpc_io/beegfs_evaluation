from commons import csv, charts
import numpy as np
import pandas as pd

path_sc1 = "../scenario_01"
path_sc2 = "../scenario_02"

format = "png"

# generating graph 6A
l2 = csv.load_all_ior([path_sc1 + "/storage_targets/01/csv/exec_02/output2.csv",
                       path_sc1 + "/storage_targets/02/csv/exec_02/output2.csv",
                       path_sc1 + "/storage_targets/03/csv/exec_02/output2.csv",
                       path_sc1 + "/storage_targets/04/csv/exec_02/output2.csv",
                       path_sc1 + "/storage_targets/05/csv/exec_02/output2.csv",
                       path_sc1 + "/storage_targets/06/csv/exec_02/output2.csv",
                       path_sc1 + "/storage_targets/07/csv/exec_02/output2.csv",
                       path_sc1 + "/storage_targets/08/csv/exec_02/output2.csv"])

# Insert column n_targets in each one of the data frames
n_targets = 1
for df in l2:
    df.insert(0, 'n_targets', n_targets)
    n_targets += 1

df2 = pd.concat(l2)

x_ex2 = df2['n_targets'].values.reshape((-1, 1))
y_bw_ex2 = df2['bw'].values.reshape((-1, 1))

charts.scatter_v1(x_ex2, y_bw_ex2, color='red', alpha=0.3,
                  ylabel='Bandwidth (MiB/s)', xlabel='Number of Storage Targets',
                  ylim=(800, 2400), grid=True, format=format, save_to=f"fig_6a.{format}")

# Generating graph 6B
l1 = csv.load_all_ior([path_sc2 + "/storage_targets/32_nodes/01/csv/exec_01/output.csv",
                       path_sc2 + "/storage_targets/32_nodes/02/csv/exec_01/output.csv",
                       path_sc2 + "/storage_targets/32_nodes/03/csv/exec_01/output.csv",
                       path_sc2 + "/storage_targets/32_nodes/04/csv/exec_01/output.csv",
                       path_sc2 + "/storage_targets/32_nodes/05/csv/exec_01/output.csv",
                       path_sc2 + "/storage_targets/32_nodes/06/csv/exec_01/output.csv",
                       path_sc2 + "/storage_targets/32_nodes/07/csv/exec_01/output.csv",
                       path_sc2 + "/storage_targets/32_nodes/08/csv/exec_01/output.csv"])

# Insert column n_targets in each one of the data frames
n_targets = 1
for df in l1:
    df.insert(0, 'n_targets', n_targets)
    n_targets += 1

df1 = pd.concat(l1)

x1 = df1['n_targets'].values.reshape((-1, 1))
y1_bw = df1['bw'].values.reshape((-1, 1))

charts.scatter_v1(x=x1, y=y1_bw,
                  alpha=.3, color='red',
                  ylabel='Bandwidth (MiB/s)',
                  xlabel='Number of Storage Targets',
                  xlim=(0, 9), format=format, save_to=f'fig_6b.{format}', grid=True)
