from commons import csv, charts

from commons import storageTargets as cmm_st
import numpy as np
import pandas as pd

path_sc1 = "../scenario_01"
path_sc2 = "../scenario_02"

format="png"

# load the storageDataframes
df1 = cmm_st.storageDataframe(df_ior=csv.load_ior(path_sc1 + '/storage_targets/01/csv/exec_02/output2.csv'),
                              df_tracker=csv.load_tracker(path_sc1 + '/storage_targets/01/csv/exec_02/tracker.csv'))

df2 = cmm_st.storageDataframe(df_ior=csv.load_ior(path_sc1 + '/storage_targets/02/csv/exec_02/output2.csv'),
                              df_tracker=csv.load_tracker(path_sc1 + '/storage_targets/02/csv/exec_02/tracker.csv'))

df3 = cmm_st.storageDataframe(df_ior=csv.load_ior(path_sc1 + '/storage_targets/03/csv/exec_02/output2.csv'),
                              df_tracker=csv.load_tracker(path_sc1 + '/storage_targets/03/csv/exec_02/tracker.csv'))

df4 = cmm_st.storageDataframe(df_ior=csv.load_ior(path_sc1 + '/storage_targets/04/csv/exec_02/output2.csv'),
                              df_tracker=csv.load_tracker(path_sc1 + '/storage_targets/04/csv/exec_02/tracker.csv'))

df5 = cmm_st.storageDataframe(df_ior=csv.load_ior(path_sc1 + '/storage_targets/05/csv/exec_02/output2.csv'),
                              df_tracker=csv.load_tracker(path_sc1 + '/storage_targets/05/csv/exec_02/tracker.csv'))

df6 = cmm_st.storageDataframe(df_ior=csv.load_ior(path_sc1 + '/storage_targets/06/csv/exec_02/output2.csv'),
                              df_tracker=csv.load_tracker(path_sc1 + '/storage_targets/06/csv/exec_02/tracker.csv'))

df7 = cmm_st.storageDataframe(df_ior=csv.load_ior(path_sc1 + '/storage_targets/07/csv/exec_02/output2.csv'),
                              df_tracker=csv.load_tracker(path_sc1 + '/storage_targets/07/csv/exec_02/tracker.csv'))

df8 = cmm_st.storageDataframe(df_ior=csv.load_ior(path_sc1 + '/storage_targets/08/csv/exec_02/output2.csv'),
                              df_tracker=csv.load_tracker(path_sc1 + '/storage_targets/08/csv/exec_02/tracker.csv'))

# Split it by balance rate
lt = []
lt.extend(cmm_st.split_by_balance_rate(df1))
lt.extend(cmm_st.split_by_balance_rate(df2))
lt.extend(cmm_st.split_by_balance_rate(df3))
lt.extend(cmm_st.split_by_balance_rate(df4))
lt.extend(cmm_st.split_by_balance_rate(df5))
lt.extend(cmm_st.split_by_balance_rate(df6))
lt.extend(cmm_st.split_by_balance_rate(df7))
lt.extend(cmm_st.split_by_balance_rate(df8))

dframes = sorted(lt, key=lambda x: x['bw'].mean())
xlabels = []
for d in dframes:
    xlabels.append(d.iloc[0]['balance_rate'])

values = [d['bw'].values for d in dframes]

charts.boxplot_v2(xlabels=xlabels, values=values,
                  xlabel='Targets Placement ($min,max$)',
                  ylabel='Bandwidth(MiB/s)',x_fontsize=22, format=format, save_to=f'fig_08.{format}')

