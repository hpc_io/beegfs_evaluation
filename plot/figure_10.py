from commons import csv, charts

from commons import storageTargets as cmm_st
import numpy as np
import pandas as pd

path_sc1 = "../scenario_01"
path_sc2 = "../scenario_02"
format="png"

l1 = csv.load_all_ior([path_sc2 + "/storage_targets/32_nodes/01/csv/exec_01/output.csv",
                       path_sc2 + "/storage_targets/32_nodes/02/csv/exec_01/output.csv",
                       path_sc2 + "/storage_targets/32_nodes/03/csv/exec_01/output.csv",
                       path_sc2 + "/storage_targets/32_nodes/04/csv/exec_01/output.csv",
                       path_sc2 + "/storage_targets/32_nodes/05/csv/exec_01/output.csv",
                       path_sc2 + "/storage_targets/32_nodes/06/csv/exec_01/output.csv",
                       path_sc2 + "/storage_targets/32_nodes/07/csv/exec_01/output.csv",
                       path_sc2 + "/storage_targets/32_nodes/08/csv/exec_01/output.csv"])

# load the storageDataframes
df1 = cmm_st.storageDataframe(df_ior=l1[0],
                              df_tracker=csv.load_tracker(
                                  path_sc2 + '/storage_targets/32_nodes/01/csv/exec_01/tracker.csv'))

df2 = cmm_st.storageDataframe(df_ior=l1[1],
                              df_tracker=csv.load_tracker(
                                  path_sc2 + '/storage_targets/32_nodes/02/csv/exec_01/tracker.csv'))

df3 = cmm_st.storageDataframe(df_ior=l1[2],
                              df_tracker=csv.load_tracker(
                                  path_sc2 + '/storage_targets/32_nodes/03/csv/exec_01/tracker.csv'))

df4 = cmm_st.storageDataframe(df_ior=l1[3],
                              df_tracker=csv.load_tracker(
                                  path_sc2 + '/storage_targets/32_nodes/04/csv/exec_01/tracker.csv'))

df5 = cmm_st.storageDataframe(df_ior=l1[4],
                              df_tracker=csv.load_tracker(
                                  path_sc2 + '/storage_targets/32_nodes/05/csv/exec_01/tracker.csv'))

df6 = cmm_st.storageDataframe(df_ior=l1[5],
                              df_tracker=csv.load_tracker(
                                  path_sc2 + '/storage_targets/32_nodes/06/csv/exec_01/tracker.csv'))

df7 = cmm_st.storageDataframe(df_ior=l1[6],
                              df_tracker=csv.load_tracker(
                                  path_sc2 + '/storage_targets/32_nodes/07/csv/exec_01/tracker.csv'))

df8 = cmm_st.storageDataframe(df_ior=l1[7],
                              df_tracker=csv.load_tracker(
                                  path_sc2 + '/storage_targets/32_nodes/08/csv/exec_01/tracker.csv'))

# Split it by balance rate
lt = []
lt.extend(cmm_st.split_by_balance_rate(df1))
lt.extend(cmm_st.split_by_balance_rate(df2))
lt.extend(cmm_st.split_by_balance_rate(df3))
lt.extend(cmm_st.split_by_balance_rate(df4))
lt.extend(cmm_st.split_by_balance_rate(df5))
lt.extend(cmm_st.split_by_balance_rate(df6))
lt.extend(cmm_st.split_by_balance_rate(df7))
lt.extend(cmm_st.split_by_balance_rate(df8))

dframes = sorted(lt, key=lambda x: x['bw'].mean())
xlabels = []
for d in dframes:
    xlabels.append(d.iloc[0]['balance_rate'])

values = [d['bw'].values for d in dframes]
import os

charts.boxplot_v2(xlabels=xlabels, values=values,
                  ylabel='Bandwidth (MB/s)',
                  ylim=(1000, 10000),
                  xlabel='Targets Placement ($min,max$)',
                  format=format,
                  save_to=f'fig_10.{format}')