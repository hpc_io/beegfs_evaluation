from commons import csv, charts

from commons import storageTargets as cmm_st
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from numpy import array

path_sc2 = "../scenario_02"

format="png"
# Load all data

def generate_xy_values(x_array, csv_list, n_lines=90):
    l = csv.load_all_ior(csv_list)

    i = 0
    for df in l:
        df.insert(0, 'n_nodes', x_array[i])
        l[i] = l[i].head(n_lines)
        i += 1

    x_values = [df.n_nodes for df in l]
    y_values = [df.bw for df in l]
    y_mean = [df.bw.mean() for df in l]

    return x_values, y_values, y_mean, l


def plot_figure(x, x1, y1, y1_mean, x2, y2, y2_mean, x4, y4, y4_mean, x8, y8, y8_mean, save_to):
    fig, ax = plt.subplots()
    fig.set_size_inches(11, 7, forward=True)

    plt.scatter(x1, y1, color='silver', alpha=.6)
    plt.plot(x, y1_mean, 'v--', color="gray", label='stripe count 1')

    plt.scatter(x2, y2, color='lightgreen', alpha=.6)
    plt.plot(x, y2_mean, 's--', color="green", label='stripe count 2')

    plt.scatter(x4, y4, alpha=.4, color='lightcoral')
    plt.plot(x, y4_mean, 'x--', color="red", label='stripe count 4')

    plt.scatter(x8, y8, alpha=.6, color='lightblue')
    plt.plot(x, y8_mean, 'o--', alpha=0.6, color="blue", label='stripe count 8')

    plt.ylabel('Bandwidth (MiB/s)', fontsize=22, fontweight='bold')
    plt.xlabel('Number of Compute Nodes', fontweight='bold', fontsize=22)

    # plt.title(title,  fontsize=18)

    ax.tick_params(axis='both', which='major', labelsize=16)
    # ax.tick_params(axis='both', which='minor', labelsize=8)
    ax.set_xticks(x)
    plt.grid()
    plt.legend(prop={'size': 16})

    fig.savefig(save_to, format=format, dpi=1200)


x = array([1, 2, 4, 8, 16, 32])
# x1 = array([1, 2, 4, 8, 16])

x1_values, y1_values, y1_mean, l1 = generate_xy_values(x, [
    path_sc2 + "/compute_nodes/1_target/01/csv/exec_01/output.csv",
    path_sc2 + "/compute_nodes/1_target/02/csv/exec_01/output.csv",
    path_sc2 + "/compute_nodes/1_target/04/csv/exec_01/output.csv",
    path_sc2 + "/compute_nodes/1_target/08/csv/exec_01/output.csv",
    path_sc2 + "/compute_nodes/1_target/16/csv/exec_01/output.csv",
    path_sc2 + "/compute_nodes/1_target/32/csv/exec_01/output.csv"])

x2_values, y2_values, y2_mean, l2 = generate_xy_values(x, [
    path_sc2 + "/compute_nodes/2_target/01/csv/exec_01/output.csv",
    path_sc2 + "/compute_nodes/2_target/02/csv/exec_01/output.csv",
    path_sc2 + "/compute_nodes/2_target/04/csv/exec_01/output.csv",
    path_sc2 + "/compute_nodes/2_target/08/csv/exec_01/output.csv",
    path_sc2 + "/compute_nodes/2_target/16/csv/exec_01/output.csv",
    path_sc2 + "/compute_nodes/2_target/32/csv/exec_01/output.csv"])

x4_values, y4_values, y4_mean, l4 = generate_xy_values(x, [
    path_sc2 + "/compute_nodes/4_targets/8_process/01/csv/exec_01/output.csv",
    path_sc2 + "/compute_nodes/4_targets/8_process/02/csv/exec_01/output.csv",
    path_sc2 + "/compute_nodes/4_targets/8_process/04/csv/exec_01/output.csv",
    path_sc2 + "/compute_nodes/4_targets/8_process/08/csv/exec_01/output.csv",
    path_sc2 + "/compute_nodes/4_targets/8_process/16/csv/exec_01/output.csv",
    path_sc2 + "/compute_nodes/4_targets/8_process/32/csv/exec_02/output.csv"],
                                                       n_lines=100)

x8_values, y8_values, y8_mean, l8 = generate_xy_values(x, [
    path_sc2 + "/compute_nodes/8_targets/01/csv/exec_02/output.csv",
    path_sc2 + "/compute_nodes/8_targets/02/csv/exec_02/output.csv",
    path_sc2 + "/compute_nodes/8_targets/04/csv/exec_02/output.csv",
    path_sc2 + "/compute_nodes/8_targets/08/csv/exec_02/output.csv",
    path_sc2 + "/compute_nodes/8_targets/16/csv/exec_02/output.csv",
    path_sc2 + "/compute_nodes/8_targets/32/csv/exec_02/output.csv"])

plot_figure(x,
            x1_values, y1_values, y1_mean,
            x2_values, y2_values, y2_mean,
            x4_values, y4_values, y4_mean,
            x8_values, y8_values, y8_mean, save_to=f'fig_11.{format}')
