from commons import csv, charts

from commons import storageTargets as cmm_st
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from collections import OrderedDict

from numpy import array

path_sc2 = "../scenario_02"


def compute_aggr_bw(df_all):
    df_all['aggr_bw'] = (df_all['volume'] / df_all['total_max'])
    return df_all


# computing
def load_apps(apps_dict: OrderedDict, tracker):
    columns_storage = [f"st_{key}" for key in apps_dict.keys()]

    tracker = tracker.groupby('tracker_id')['storage_targets'].apply('|'.join).reset_index()
    tracker[columns_storage] = tracker['storage_targets'].str.split('|', len(columns_storage) - 1, expand=True)

    for column in columns_storage:
        tracker[column] = tracker[column].apply(lambda x: set(x.split()))

    columns_df = ['bw', 'block', 'start', 'end', 'total']

    df_list = []

    start_columns = []
    end_columns = []

    total_columns = []

    for key, df in apps_dict.items():
        aux = df[columns_df]
        aux = aux.rename(columns={'bw': f"bw_{key}", 'block': f'block_{key}', 'total': f'total_{key}',
                                  'start': f'start_{key}',
                                  'end': f'end_{key}'})

        start_columns.append(f'start_{key}')
        end_columns.append(f'end_{key}')
        total_columns.append(f'total_{key}')

        df_list.append(aux)

    df_all = pd.concat(df_list, axis=1, join="inner")
    df_all = pd.concat([df_all, tracker], axis=1, join='inner')

    # compute volume
    df_all['volume'] = 0.0
    for key, df in apps_dict.items():
        df_all[f'vol_{key}'] = (df_all[f'block_{key}'] / 1024.0) * 64  # MB
        df_all['volume'] = df_all['volume'] + df_all[f'vol_{key}']

    # define total max
    df_all['total_max_1'] = df_all[total_columns].max(axis=1)
    df_all['start_min'] = df_all[start_columns].min(axis=1)
    df_all['end_max'] = df_all[end_columns].max(axis=1)

    #df_all['total_max_2'] = (df_all.end_max - df_all.start_min).apply(lambda x: x.seconds)

    df_all['aggr_bw1'] = (df_all['volume'] / df_all['total_max_1'])

    #df_all['aggr_bw2'] = (df_all['volume'] / df_all['total_max_2'])

    df_all['n_targets'] = df_all['st_a'].apply(lambda x: len(x))

    return df_all


df2_2app = load_apps(OrderedDict({'a': csv.load_ior(path_sc2 + "/overlap/2_apps/02/csv/exec_01/output_appA.csv"),
                                  'b': csv.load_ior(path_sc2 + "/overlap/2_apps/02/csv/exec_01/output_appB.csv")}),
                     csv.load_tracker(path_sc2 + "/overlap/2_apps/02/raw_output/exec_01/tracker.csv"))

df4_2app = load_apps(OrderedDict({'a': csv.load_ior(path_sc2 + "/overlap/2_apps/04/csv/exec_01/output_appA.csv"),
                                  'b': csv.load_ior(path_sc2 + "/overlap/2_apps/04/csv/exec_01/output_appB.csv")}),
                     csv.load_tracker(path_sc2 + "/overlap/2_apps/04/raw_output/exec_01/tracker.csv"))

df8_2app = load_apps(OrderedDict({'a': csv.load_ior(path_sc2 + "/overlap/2_apps/08/csv/exec_01/output_appA.csv"),
                                  'b': csv.load_ior(path_sc2 + "/overlap/2_apps/08/csv/exec_01/output_appB.csv")}),
                     csv.load_tracker(path_sc2 + "/overlap/2_apps/08/raw_output/exec_01/tracker.csv"))

df2_3app = load_apps(OrderedDict({'a': csv.load_ior(path_sc2 + "/overlap/3_apps/02/csv/exec_01/output_appA.csv"),
                                  'b': csv.load_ior(path_sc2 + "/overlap/3_apps/02/csv/exec_01/output_appB.csv"),
                                  'c': csv.load_ior(path_sc2 + "/overlap/3_apps/02/csv/exec_01/output_appC.csv")}),
                     csv.load_tracker(path_sc2 + "/overlap/3_apps/02/raw_output/exec_01/tracker.csv"))

df4_3app = load_apps(OrderedDict({'a': csv.load_ior(path_sc2 + "/overlap/3_apps/04/csv/exec_01/output_appA.csv"),
                                  'b': csv.load_ior(path_sc2 + "/overlap/3_apps/04/csv/exec_01/output_appB.csv"),
                                  'c': csv.load_ior(path_sc2 + "/overlap/3_apps/04/csv/exec_01/output_appC.csv")}),
                     csv.load_tracker(path_sc2 + "/overlap/3_apps/04/raw_output/exec_01/tracker.csv"))

df8_3app = load_apps(OrderedDict({'a': csv.load_ior(path_sc2 + "/overlap/3_apps/08/csv/exec_01/output_appA.csv"),
                                  'b': csv.load_ior(path_sc2 + "/overlap/3_apps/08/csv/exec_01/output_appB.csv"),
                                  'c': csv.load_ior(path_sc2 + "/overlap/3_apps/08/csv/exec_01/output_appC.csv")}),
                     csv.load_tracker(path_sc2 + "/overlap/3_apps/08/raw_output/exec_01/tracker.csv"))

df2a_2app = csv.load_ior(path_sc2 + "/overlap/2_apps/02/csv/exec_01/output_appA.csv")
df2b_2app = csv.load_ior(path_sc2 + "/overlap/2_apps/02/csv/exec_01/output_appB.csv")

df4a_2app = csv.load_ior(path_sc2 + "/overlap/2_apps/04/csv/exec_01/output_appA.csv")
df4b_2app = csv.load_ior(path_sc2 + "/overlap/2_apps/04/csv/exec_01/output_appB.csv")

df8a_2app = csv.load_ior(path_sc2 + "/overlap/2_apps/08/csv/exec_01/output_appA.csv")
df8b_2app = csv.load_ior(path_sc2 + "/overlap/2_apps/08/csv/exec_01/output_appB.csv")

df2_4app = load_apps(OrderedDict({'a': csv.load_ior(path_sc2 + "/overlap/4_apps/02/csv/exec_02/output_appA.csv"),
                                  'b': csv.load_ior(path_sc2 + "/overlap/4_apps/02/csv/exec_02/output_appB.csv"),
                                  'c': csv.load_ior(path_sc2 + "/overlap/4_apps/02/csv/exec_02/output_appC.csv"),
                                  'd': csv.load_ior(path_sc2 + "/overlap/4_apps/02/csv/exec_02/output_appD.csv")}),
                     csv.load_tracker(path_sc2 + "/overlap/4_apps/02/raw_output/exec_02/tracker.csv"))

df4_4app = load_apps(OrderedDict({"a": csv.load_ior(path_sc2 + "/overlap/4_apps/04/csv/exec_02/output_appA.csv"),
                                  "b": csv.load_ior(path_sc2 + "/overlap/4_apps/04/csv/exec_02/output_appB.csv"),
                                  "c": csv.load_ior(path_sc2 + "/overlap/4_apps/04/csv/exec_02/output_appC.csv"),
                                  "d": csv.load_ior(path_sc2 + "/overlap/4_apps/04/csv/exec_02/output_appD.csv")}),
                     csv.load_tracker(path_sc2 + "/overlap/4_apps/04/raw_output/exec_02/tracker.csv"))

df8_4app = load_apps(OrderedDict({"a": csv.load_ior(path_sc2 + "/overlap/4_apps/08/csv/exec_02/output_appA.csv"),
                                  "b": csv.load_ior(path_sc2 + "/overlap/4_apps/08/csv/exec_02/output_appB.csv"),
                                  "c": csv.load_ior(path_sc2 + "/overlap/4_apps/08/csv/exec_02/output_appC.csv"),
                                  "d": csv.load_ior(path_sc2 + "/overlap/4_apps/08/csv/exec_02/output_appD.csv")}),
                     csv.load_tracker(path_sc2 + "/overlap/4_apps/08/raw_output/exec_02/tracker.csv"))

dfsingle_8_2 = csv.load_ior(path_sc2 + "/compute_nodes/2_target/08/csv/exec_01/output.csv")
dfsingle_16_4 = csv.load_ior(path_sc2 + "/compute_nodes/4_targets/8_process/16/csv/exec_01/output.csv")
dfsingle_8_4 = csv.load_ior(path_sc2 + "/compute_nodes/4_targets/8_process/08/csv/exec_01/output.csv")
dfsingle_16_8 = csv.load_ior(path_sc2 + "/compute_nodes/8_targets/16/csv/exec_02/output.csv")
dfsingle_8_8 = csv.load_ior(path_sc2 + "/compute_nodes/8_targets/08/csv/exec_02/output.csv")

df2a_3app = csv.load_ior(path_sc2 + "/overlap/3_apps/02/csv/exec_01/output_appA.csv")
df2b_3app = csv.load_ior(path_sc2 + "/overlap/3_apps/02/csv/exec_01/output_appB.csv")
df2c_3app = csv.load_ior(path_sc2 + "/overlap/3_apps/02/csv/exec_01/output_appC.csv")

df8a_3app = csv.load_ior(path_sc2 + "/overlap/3_apps/08/csv/exec_01/output_appA.csv")
df8b_3app = csv.load_ior(path_sc2 + "/overlap/3_apps/08/csv/exec_01/output_appB.csv")
df8c_3app = csv.load_ior(path_sc2 + "/overlap/3_apps/08/csv/exec_01/output_appC.csv")

df2a_4app = csv.load_ior(path_sc2 + "/overlap/4_apps/02/csv/exec_02/output_appA.csv")
df2b_4app = csv.load_ior(path_sc2 + "/overlap/4_apps/02/csv/exec_02/output_appB.csv")
df2c_4app = csv.load_ior(path_sc2 + "/overlap/4_apps/02/csv/exec_02/output_appC.csv")
df2d_4app = csv.load_ior(path_sc2 + "/overlap/4_apps/02/csv/exec_02/output_appD.csv")

df8a_4app = csv.load_ior(path_sc2 + "/overlap/4_apps/08/csv/exec_02/output_appA.csv")
df8b_4app = csv.load_ior(path_sc2 + "/overlap/4_apps/08/csv/exec_02/output_appB.csv")
df8c_4app = csv.load_ior(path_sc2 + "/overlap/4_apps/08/csv/exec_02/output_appC.csv")
df8d_4app = csv.load_ior(path_sc2 + "/overlap/4_apps/08/csv/exec_02/output_appD.csv")

dfsingle_32_8 = csv.load_ior(path_sc2 + "/compute_nodes/8_targets/32/csv/exec_02/output.csv")
dfsingle_32_6 = csv.load_ior(path_sc2 + "/storage_targets/32_nodes/06/csv/exec_01/output.csv")

ylabel_fontsize = 50
xtick_labelsize = 48

fig_weight = 36
fig_height = 20.5


def figure_12a():
    labels = ['Single \napp \n8 nodes \n2 OST',
              '2 apps \n8 nodes\n 2 OST',
              'Aggregated\n 2 apps\n 8 nodes\n 2 OST',
              'Single \napp\n 16 nodes\n 4 OST',
              'Single \napp\n 8 nodes\n 4 OST',
              '2 apps\n 8 nodes\n 4 OST',
              'Aggregated\n 2 apps\n 8 nodes\n 4 OST',
              'Single \napp \n16 nodes \n8 OST',
              'Single \napp \n8 nodes \n8 OST',
              '2 apps \n8 nodes\n 8 OST',
              'Aggregated\n 2 apps \n8 nodes\n 8 OST']

    aggr_bw = [
        0,
        0,
        df2_2app.aggr_bw1.mean(),
        0,
        0,
        0,
        df4_2app.aggr_bw1.mean(),
        0,
        0,
        0,
        df8_2app.aggr_bw1.mean()
    ]

    appa_means = [0,
                  df2a_2app.bw.mean(),
                  0,
                  0,
                  0,
                  df4a_2app.bw.mean(),
                  0,
                  0,
                  0,
                  df8a_2app.bw.mean(),
                  0
                  ]

    appb_means = [0,
                  df2b_2app.bw.mean(),
                  0,
                  0,
                  0,
                  df4b_2app.bw.mean(),
                  0,
                  0,
                  0,
                  df8b_2app.bw.mean(),
                  0
                  ]

    single_means = [dfsingle_8_2.bw.mean(),
                    0,
                    0,
                    dfsingle_16_4.bw.mean(),
                    dfsingle_8_4.bw.mean(),
                    0,
                    0,
                    dfsingle_16_8.bw.mean(),
                    dfsingle_8_8.bw.mean(),
                    0,
                    0
                    ]

    width = 0.35  # the width of the bars: can also be len(x) sequence

    fig, ax = plt.subplots()

    fig.set_size_inches(54.5, 20.5)

    plt.ylim((0, 11000))

    ax.bar(labels, aggr_bw, width, label='aggregated Bw', color='red', hatch='', edgecolor="black", linewidth=2)

    ax.bar(labels, single_means, width, label='single', hatch='', edgecolor="black", linewidth=2)

    ax.bar(labels, appa_means, width, label='App A', color='lightcoral', hatch='x', edgecolor="black", linewidth=2)
    ax.bar(labels, appb_means, width, bottom=appa_means, label='App B', color='lightcoral', hatch='/', linewidth=2,
           edgecolor="black")
    plt.subplots_adjust(bottom=0.18)

    plt.ylabel('Bandwidth (MiB/s)', fontsize=50, fontweight='bold')

    ax.tick_params(axis='both', which='major', labelsize=48)
    # plt.legend( loc="upper left", prop={'size': 14})
    plt.gca().yaxis.grid(True)

    fig.savefig("fig_12a.pdf", format='pdf', dpi=1200)
 

def figure_12b():
    labels = [
        'Single \napp \n8 nodes \n2 OST',

        '3 apps \n8 nodes\n 2 OST',
        'Aggregated\n 3 apps\n 8 nodes\n 2 OST',
        'Single \napp\n 24 nodes\n 6 OST',
        'Single \napp\n 8 nodes\n 8 OST',

        '3 apps \n8 nodes\n 8 OST',
        'Aggregated\n 3 apps \n8 nodes\n 8 OST',
        'Single \napp\n 24 nodes\n 8 OST']

    aggr_bw_3app = [
        0,
        0,
        df2_3app.aggr_bw1.mean(),
        0,
        0,
        0,
        df8_3app.aggr_bw1.mean(),
        0
    ]

    A_3app = np.array([
        0,
        df2a_3app.bw.mean(),
        0,
        0,
        0,
        df8a_3app.bw.mean(),
        0,
        0
    ])

    B_3app = np.array([
        0,
        df2b_3app.bw.mean(),
        0,
        0,
        0,
        df8b_3app.bw.mean(),
        0,
        0
    ])

    C_3app = np.array([
        0,
        df2c_3app.bw.mean(),
        0,
        0,
        0,
        df8c_3app.bw.mean(),
        0,
        0
    ])

    single = np.array([
        dfsingle_8_2.bw.mean(),
        0,
        0,
        7085, 
        dfsingle_8_8.bw.mean(),
        0,
        0,
        8100  
    ])

    width = 0.35  # the width of the bars: can also be len(x) sequence

    fig, ax = plt.subplots()
    fig.set_size_inches(fig_weight, fig_height)
    ax.bar(labels, aggr_bw_3app, width, label='aggregated Bw 3app', color='red', hatch='', edgecolor="black",
           linewidth=2)
    plt.ylim((0, 11000))
    ax.bar(labels, A_3app, width, label='App A (3app)', color='lightcoral', hatch='x', edgecolor="black", linewidth=2)
    ax.bar(labels, B_3app, width, bottom=A_3app, label='App B (3app)', color='lightcoral', hatch='/', edgecolor="black",
           linewidth=2)
    ax.bar(labels, C_3app, width, bottom=A_3app + B_3app, label='App C (3app)', color='lightcoral', hatch='+',
           edgecolor="black", linewidth=2)

    ax.bar(labels, single, width, label='single app', hatch='', edgecolor="black", linewidth=2)
    plt.subplots_adjust(bottom=0.18)

    plt.ylabel('Bandwidth (MiB/s)', fontsize=ylabel_fontsize, fontweight='bold')
    ax.tick_params(axis='both', which='major', labelsize=xtick_labelsize)
    # plt.legend( loc="upper right", prop={'size': 14})
    plt.gca().yaxis.grid(True)

    fig.savefig("fig_12b.pdf", format='pdf', dpi=1200)


def figure_12c():
    labels = [
        'Single \napp \n8 nodes \n2 OST',

        '4 apps \n8 nodes\n 2 OST',
        'Aggregated\n 4 apps\n 8 nodes\n 2 OST',

        'Single \napp\n 32 nodes\n 8 OST',

        'Single \napp\n 8 nodes\n 8 OST',

        '4 apps \n8 nodes\n 8 OST',
        'Aggregated\n 4 apps \n8 nodes\n 8 OST']

    aggr_bw_4app = [
        0,
        0,
        df2_4app.aggr_bw1.mean(),
        0,
        0,
        0,
        df8_4app.aggr_bw1.mean(),

    ]

    A_4app = np.array([
        0,
        df2a_4app.bw.mean(),
        0,
        0,
        0,
        df8a_4app.bw.mean(),
        0

    ])

    B_4app = np.array([
        0,
        df2b_4app.bw.mean(),
        0,
        0,
        0,
        df8b_4app.bw.mean(),
        0

    ])

    C_4app = np.array([
        0,
        df2c_4app.bw.mean(),
        0,
        0,
        0,
        df8c_4app.bw.mean(),
        0

    ])

    D_4app = np.array([
        0,
        df2d_4app.bw.mean(),
        0,
        0,
        0,
        df8d_4app.bw.mean(),
        0

    ])

    single = np.array([
        dfsingle_8_2.bw.mean(),
        0,
        0,
        dfsingle_32_8.bw.mean(),
        dfsingle_8_8.bw.mean(),
        0,
        0,

    ])

    width = 0.35  # the width of the bars: can also be len(x) sequence

    fig, ax = plt.subplots()
    fig.set_size_inches(fig_weight, fig_height)

    ax.bar(labels, aggr_bw_4app, width, label='aggregated Bw 3app', color='red', hatch='', edgecolor="black",
           linewidth=2)
    plt.ylim((0, 11000))

    plt.subplots_adjust(bottom=0.18)

    ax.bar(labels, A_4app, width, label='App A (4app)', color='lightcoral', hatch='x', edgecolor="black", linewidth=2)
    ax.bar(labels, B_4app, width, bottom=A_4app, label='App B (4app)', color='lightcoral', hatch='/', edgecolor="black",
           linewidth=2)
    ax.bar(labels, C_4app, width, bottom=A_4app + B_4app, label='App C (4app)', color='lightcoral', hatch='+',
           edgecolor="black", linewidth=2)
    ax.bar(labels, D_4app, width, bottom=A_4app + B_4app + C_4app, label='App D (4app)', color='lightcoral', hatch='o',
           edgecolor="black", linewidth=2)

    ax.bar(labels, single, width, label='single app', hatch='', edgecolor="black", linewidth=2)

    plt.ylabel('Bandwidth (MiB/s)', fontsize=ylabel_fontsize, fontweight='bold')
    ax.tick_params(axis='both', which='major', labelsize=xtick_labelsize)
    # plt.legend( loc="upper right", prop={'size': 14})
    plt.gca().yaxis.grid(True)

    fig.savefig("fig_12c.pdf", format='pdf', dpi=1200)


def figure_extra():
    labels = ['Aggregated\n 2 apps\n 8 nodes\n 2 OST',
              'Single \napp\n 16 nodes\n 4 OST',

              'Aggregated\n 3 apps\n 8 nodes\n 2 OST',
              'Single \napp\n 24 nodes\n 6 OST',

              'Aggregated\n 4 apps\n 8 nodes\n 2 OST',
              'Single \napp\n 32 nodes\n 8 OST'
           ]

    aggr_bw = [
        df2_2app.aggr_bw1.mean(),
        0,
        df2_3app.aggr_bw1.mean(),
        0,
        df2_4app.aggr_bw1.mean(),
        0
    ]
  

    single_means = [
        0,
        dfsingle_16_4.bw.mean(),
        0,
        7085,  
        0,
        dfsingle_32_8.bw.mean()]

    width = 0.35  # the width of the bars: can also be len(x) sequence

    fig, ax = plt.subplots()

    fig.set_size_inches(20, 9.5)

    plt.ylim((0, 11000))

    ax.bar(labels, aggr_bw, width, label='aggregated Bw', color='red', hatch='', edgecolor="black", linewidth=2)

    ax.bar(labels, single_means, width, label='single', hatch='', color='blue', edgecolor="black", linewidth=2)

    plt.subplots_adjust(bottom=0.3)

    plt.ylabel('Bandwidth (MiB/s)', fontsize=26, fontweight='bold')

    ax.tick_params(axis='both', which='major', labelsize=24)
    # plt.legend( loc="upper left", prop={'size': 14})
    plt.gca().yaxis.grid(True)

    fig.savefig("fig_extra.png", format='png', dpi=1200)
 


figure_12a()
figure_12b()
figure_12c()


figure_extra()
