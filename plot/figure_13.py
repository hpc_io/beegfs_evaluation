from commons import csv, charts

from commons import storageTargets as cmm_st
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from collections import OrderedDict

from numpy import array

path_sc2 = "../scenario_02"

df = pd.read_csv(path_sc2 + '/overlap/2_apps/individual_2apps_4ost.csv')

values = [
    df.groupby('allsame').get_group(False).bw.values,
    df.groupby('allsame').get_group(True).bw.values
]

xlabels = ['Sharing OST', 'Not sharing OST']
charts.boxplot_v2(xlabels=xlabels, values=values,
                  ylabel='Bandwidth (MiB/s)',
                  xtick_fontsize=28,
                  y_fontsize=30,
                  plot_weight=13,
                  format="png",
                  save_to=f'fig_13.png')
