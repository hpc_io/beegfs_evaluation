#!/usr/bin/env bash

        # ############################################################
        # This script was generated using hourglass 1.0.0
        # Date: 2022-02-04
        # Used Parameters:
        #./hourglass  --cmd_file 'commands.txt' 
        #	--r 5 --s 1.0 --e 10.0 
        #	--o launcher.sh --u min
        #	--m False --v True
        # ############################################################

 
    run () {
        start_time="$(date -u +%s)"
        echo "Starting Execution ${1}...$(date)"
        eval ${2}
        end_time="$(date -u +%s)"
        elapsed="$(($end_time-$start_time))"
        echo "Execution ${1} finished at $(date). Elapsed time ${elapsed}"                
        echo "Sleep time for next execution:  ${3}"
        sleep "${4}"
        echo 
    }


run 0 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "6.8 min" 409
run 1 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "2.1 min" 128
run 2 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "8.8 min" 526
run 3 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "4.6 min" 277
run 4 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "0.0 min" 0.0
run 5 "(echo 'running 16'; cd 16/; sbatch -W slurm_start.sh)" "7.1 min" 424
run 6 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "9.4 min" 566
run 7 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "7.6 min" 458
run 8 "(echo 'running 16'; cd 16/; sbatch -W slurm_start.sh)" "5.0 min" 302
run 9 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "4.9 min" 294
run 10 "(echo 'running 01'; cd 01/; sbatch -W slurm_start.sh)" "1.4 min" 84
run 11 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "5.1 min" 306
run 12 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "9.9 min" 592
run 13 "(echo 'running 01'; cd 01/; sbatch -W slurm_start.sh)" "7.2 min" 429
run 14 "(echo 'running 16'; cd 16/; sbatch -W slurm_start.sh)" "8.6 min" 513
run 15 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "3.1 min" 187
run 16 "(echo 'running 01'; cd 01/; sbatch -W slurm_start.sh)" "2.0 min" 120
run 17 "(echo 'running 16'; cd 16/; sbatch -W slurm_start.sh)" "3.0 min" 181
run 18 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "9.0 min" 541
run 19 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "1.8 min" 106
run 20 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "1.7 min" 100
run 21 "(echo 'running 16'; cd 16/; sbatch -W slurm_start.sh)" "2.0 min" 118
run 22 "(echo 'running 01'; cd 01/; sbatch -W slurm_start.sh)" "5.1 min" 306
run 23 "(echo 'running 01'; cd 01/; sbatch -W slurm_start.sh)" "9.4 min" 562
run 24 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "6.1 min" 367
