#!/usr/bin/env bash
##########################
#That script executes IOR
##########################

#SBATCH --job-name=.home.luan.Devel.luan-teylo.tests.omnipath.compute_nodes.8_targets.32

# run for five minutes
#              d-hh:mm:ss
#SBATCH --time=0-5:00:00


##  standard output message
#SBATCH -o batch%j.out
# # output error message
#SBATCH -e batch%j.err
#SBATCH --exclusive -C miriel
#SBATCH -N 32 -n 256


##

module purge
module load mpi/openmpi/4.0.3-mlx

echo "===== My JOB Information ===="
echo "Node List:  $SLURM_NODELIST"
echo "my jobID:  $SLURM_JOB_ID"
echo "Partition: $SLURM_JOB_PARTITION"
echo "submit directory:  $SLURM_SUBMIT_DIR"
echo "submit host:  $SLURM_SUBMIT_HOST"
echo "In the directory: $(pwd)"
echo "As the user: $(whoami)"
echo "============================"

export PATH=$PATH:/home/lgouveia/ior/bin
export PATH=$PATH:/home/lgouveia/luan-teylo/code

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/lgouveia/ior/lib

## PATH where IOR will write the file
IOR_PATH=/beegfs/lgouveia/ior_4

# Environment PER IOR Instance
n_tasks=256
n_tasks_per_node=8

# IOR PARAMETERS
parameters='-w -t 1m -b 128m -k'

# Number of executions
n=10
sleep_time=60 # time between consecutive executions



# Perform the execution block $n times.
x=1
while [ $x -le $n ]
do
  ## Aggregate Execution
  srun  --ntasks="${n_tasks}" --ntasks-per-node="${n_tasks_per_node}" ior ${parameters} -o ${IOR_PATH}/testFile1

  ## Call File Tracker
  file_tracker.py ${IOR_PATH}  --v

  sleep ${sleep_time}

  x=$(( $x + 1 ))
done


