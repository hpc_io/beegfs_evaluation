#!/usr/bin/env bash

        # ############################################################
        # This script was generated using hourglass 1.0.0
        # Date: 2022-02-01
        # Used Parameters:
        #./hourglass  --cmd_file 'commands.txt' 
        #	--r 10 --s 1.0 --e 30.0 
        #	--o launcher.sh --u min
        #	--m False --v True
        # ############################################################

 
    run () {
        start_time="$(date -u +%s)"
        echo "Starting Execution ${1}...$(date)"
        eval ${2}
        end_time="$(date -u +%s)"
        elapsed="$(($end_time-$start_time))"
        echo "Execution ${1} finished at $(date). Elapsed time ${elapsed}"                
        echo "Sleep time for next execution:  ${3}"
        sleep "${4}"
        echo 
    }


run 0 "(echo 'running 32'; cd 32/; sbatch -W slurm_start.sh)" "21.9 min" 1311
run 1 "(echo 'running 32'; cd 32/; sbatch -W slurm_start.sh)" "1.8 min" 110
run 2 "(echo 'running 32'; cd 32/; sbatch -W slurm_start.sh)" "10.0 min" 602
run 3 "(echo 'running 32'; cd 32/; sbatch -W slurm_start.sh)" "24.8 min" 1490
run 4 "(echo 'running 32'; cd 32/; sbatch -W slurm_start.sh)" "24.8 min" 1488
run 5 "(echo 'running 32'; cd 32/; sbatch -W slurm_start.sh)" "29.1 min" 1744
run 6 "(echo 'running 32'; cd 32/; sbatch -W slurm_start.sh)" "3.8 min" 229
run 7 "(echo 'running 32'; cd 32/; sbatch -W slurm_start.sh)" "29.2 min" 1750
run 8 "(echo 'running 32'; cd 32/; sbatch -W slurm_start.sh)" "28.8 min" 1727
run 9 "(echo 'running 32'; cd 32/; sbatch -W slurm_start.sh)" "0.0 min" 0.0
