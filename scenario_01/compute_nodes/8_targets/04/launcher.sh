!/usr/bin/env bash

        # ############################################################
        # This script was generated using hourglass 1.0.0
        # Date: 2021-06-09
        # Used Parameters:
        #./hourglass 'sbatch -W slurm_start.sh' 
        #	--r 10 --s 1.0 --e 10.0 
        #	--o launcher.sh --u min
        #	--m False --v False
        # ############################################################

 
    run () {
        start_time="$(date -u +%s)"
        echo "Starting Execution ${1}...$(date)"
        ${2}
        end_time="$(date -u +%s)"
        elapsed="$(($end_time-$start_time))"
        echo "Execution ${1} finished at $(date). Elapsed time ${elapsed}"                
        echo "Sleep time for next execution:  ${3}"
        sleep "${4}"
        echo 
    }


run 0 'sbatch -W slurm_start.sh' '4.6 min' 276
run 1 'sbatch -W slurm_start.sh' '6.2 min' 374
run 2 'sbatch -W slurm_start.sh' '7.8 min' 468
run 3 'sbatch -W slurm_start.sh' '7.2 min' 433
run 4 'sbatch -W slurm_start.sh' '9.5 min' 569
run 5 'sbatch -W slurm_start.sh' '4.6 min' 276
run 6 'sbatch -W slurm_start.sh' '3.2 min' 194
run 7 'sbatch -W slurm_start.sh' '8.6 min' 514
run 8 'sbatch -W slurm_start.sh' '8.5 min' 511
run 9 'sbatch -W slurm_start.sh' '0.0 min' 0.0
