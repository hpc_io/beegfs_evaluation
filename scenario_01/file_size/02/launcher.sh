#!/usr/bin/env bash

        # ############################################################
        # This script was generated using hourglass 1.0.0
        # Date: 2021-12-09
        # Used Parameters:
        #./hourglass '['sbatch -W slurm_start.sh']' 
        #	--r 10 --s 1.0 --e 5.0 
        #	--o 02/launcher.sh --u min
        #	--m False --v True
        # ############################################################

 
    run () {
        start_time="$(date -u +%s)"
        echo "Starting Execution ${1}...$(date)"
        ${2}
        end_time="$(date -u +%s)"
        elapsed="$(($end_time-$start_time))"
        echo "Execution ${1} finished at $(date). Elapsed time ${elapsed}"                
        echo "Sleep time for next execution:  ${3}"
        sleep "${4}"
        echo 
    }


run 0 'sbatch -W slurm_start.sh' '4.7 min' 281
run 1 'sbatch -W slurm_start.sh' '1.2 min' 73
run 2 'sbatch -W slurm_start.sh' '3.7 min' 224
run 3 'sbatch -W slurm_start.sh' '4.5 min' 270
run 4 'sbatch -W slurm_start.sh' '4.9 min' 294
run 5 'sbatch -W slurm_start.sh' '1.9 min' 117
run 6 'sbatch -W slurm_start.sh' '2.6 min' 156
run 7 'sbatch -W slurm_start.sh' '1.2 min' 71
run 8 'sbatch -W slurm_start.sh' '4.2 min' 254
run 9 'sbatch -W slurm_start.sh' '0.0 min' 0.0
