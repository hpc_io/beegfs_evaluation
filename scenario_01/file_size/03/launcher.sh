#!/usr/bin/env bash

        # ############################################################
        # This script was generated using hourglass 1.0.0
        # Date: 2021-12-09
        # Used Parameters:
        #./hourglass '['sbatch -W slurm_start.sh']' 
        #	--r 10 --s 1.0 --e 5.0 
        #	--o 03/launcher.sh --u min
        #	--m False --v True
        # ############################################################

 
    run () {
        start_time="$(date -u +%s)"
        echo "Starting Execution ${1}...$(date)"
        ${2}
        end_time="$(date -u +%s)"
        elapsed="$(($end_time-$start_time))"
        echo "Execution ${1} finished at $(date). Elapsed time ${elapsed}"                
        echo "Sleep time for next execution:  ${3}"
        sleep "${4}"
        echo 
    }


run 0 'sbatch -W slurm_start.sh' '1.1 min' 63
run 1 'sbatch -W slurm_start.sh' '4.6 min' 276
run 2 'sbatch -W slurm_start.sh' '1.5 min' 90
run 3 'sbatch -W slurm_start.sh' '3.1 min' 186
run 4 'sbatch -W slurm_start.sh' '4.6 min' 275
run 5 'sbatch -W slurm_start.sh' '3.4 min' 205
run 6 'sbatch -W slurm_start.sh' '2.6 min' 157
run 7 'sbatch -W slurm_start.sh' '3.0 min' 183
run 8 'sbatch -W slurm_start.sh' '3.5 min' 211
run 9 'sbatch -W slurm_start.sh' '0.0 min' 0.0
