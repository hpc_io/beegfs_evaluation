#!/usr/bin/env bash

        # ############################################################
        # This script was generated using hourglass 1.0.0
        # Date: 2021-12-09
        # Used Parameters:
        #./hourglass '['sbatch -W slurm_start.sh']' 
        #	--r 10 --s 1.0 --e 10.0 
        #	--o 07/launcher.sh --u min
        #	--m False --v True
        # ############################################################

 
    run () {
        start_time="$(date -u +%s)"
        echo "Starting Execution ${1}...$(date)"
        ${2}
        end_time="$(date -u +%s)"
        elapsed="$(($end_time-$start_time))"
        echo "Execution ${1} finished at $(date). Elapsed time ${elapsed}"                
        echo "Sleep time for next execution:  ${3}"
        sleep "${4}"
        echo 
    }


run 0 'sbatch -W slurm_start.sh' '8.3 min' 497
run 1 'sbatch -W slurm_start.sh' '1.4 min' 81
run 2 'sbatch -W slurm_start.sh' '8.2 min' 489
run 3 'sbatch -W slurm_start.sh' '4.2 min' 249
run 4 'sbatch -W slurm_start.sh' '4.5 min' 269
run 5 'sbatch -W slurm_start.sh' '7.4 min' 446
run 6 'sbatch -W slurm_start.sh' '5.5 min' 330
run 7 'sbatch -W slurm_start.sh' '4.3 min' 261
run 8 'sbatch -W slurm_start.sh' '6.8 min' 407
run 9 'sbatch -W slurm_start.sh' '0.0 min' 0.0
