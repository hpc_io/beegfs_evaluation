#!/usr/bin/env bash

        # ############################################################
        # This script was generated using hourglass 1.0.0
        # Date: 2021-12-09
        # Used Parameters:
        #./hourglass '['sbatch -W slurm_start.sh']' 
        #	--r 10 --s 1.0 --e 10.0 
        #	--o 08/launcher.sh --u min
        #	--m False --v True
        # ############################################################

 
    run () {
        start_time="$(date -u +%s)"
        echo "Starting Execution ${1}...$(date)"
        ${2}
        end_time="$(date -u +%s)"
        elapsed="$(($end_time-$start_time))"
        echo "Execution ${1} finished at $(date). Elapsed time ${elapsed}"                
        echo "Sleep time for next execution:  ${3}"
        sleep "${4}"
        echo 
    }


run 0 'sbatch -W slurm_start.sh' '7.3 min' 436
run 1 'sbatch -W slurm_start.sh' '6.0 min' 358
run 2 'sbatch -W slurm_start.sh' '5.5 min' 327
run 3 'sbatch -W slurm_start.sh' '7.6 min' 454
run 4 'sbatch -W slurm_start.sh' '5.6 min' 337
run 5 'sbatch -W slurm_start.sh' '8.3 min' 498
run 6 'sbatch -W slurm_start.sh' '1.0 min' 62
run 7 'sbatch -W slurm_start.sh' '6.3 min' 380
run 8 'sbatch -W slurm_start.sh' '2.3 min' 140
run 9 'sbatch -W slurm_start.sh' '0.0 min' 0.0
