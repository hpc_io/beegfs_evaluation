#!/usr/bin/env bash

        # ############################################################
        # This script was generated using hourglass 1.0.0
        # Date: 2022-02-05
        # Used Parameters:
        #./hourglass  --cmd_file 'commands.txt' 
        #	--r 5 --s 1.0 --e 5.0 
        #	--o launcher.sh --u min
        #	--m False --v True
        # ############################################################

 
    run () {
        start_time="$(date -u +%s)"
        echo "Starting Execution ${1}...$(date)"
        eval ${2}
        end_time="$(date -u +%s)"
        elapsed="$(($end_time-$start_time))"
        echo "Execution ${1} finished at $(date). Elapsed time ${elapsed}"                
        echo "Sleep time for next execution:  ${3}"
        sleep "${4}"
        echo 
    }


run 0 "(echo 'running 16'; cd 16/; sbatch -W slurm_start.sh)" "1.0 min" 62
run 1 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "1.9 min" 116
run 2 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "2.0 min" 122
run 3 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "1.3 min" 78
run 4 "(echo 'running 16'; cd 16/; sbatch -W slurm_start.sh)" "0.0 min" 0.0
run 5 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "2.3 min" 136
run 6 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "3.6 min" 215
run 7 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "4.1 min" 246
run 8 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "4.1 min" 245
run 9 "(echo 'running 32'; cd 32/; sbatch -W slurm_start.sh)" "3.6 min" 214
run 10 "(echo 'running 01'; cd 01/; sbatch -W slurm_start.sh)" "4.0 min" 237
run 11 "(echo 'running 32'; cd 32/; sbatch -W slurm_start.sh)" "3.6 min" 215
run 12 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "2.0 min" 122
run 13 "(echo 'running 01'; cd 01/; sbatch -W slurm_start.sh)" "1.8 min" 109
run 14 "(echo 'running 01'; cd 01/; sbatch -W slurm_start.sh)" "2.6 min" 159
run 15 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "2.1 min" 124
run 16 "(echo 'running 32'; cd 32/; sbatch -W slurm_start.sh)" "2.3 min" 139
run 17 "(echo 'running 16'; cd 16/; sbatch -W slurm_start.sh)" "3.5 min" 210
run 18 "(echo 'running 01'; cd 01/; sbatch -W slurm_start.sh)" "2.5 min" 148
run 19 "(echo 'running 32'; cd 32/; sbatch -W slurm_start.sh)" "1.4 min" 86
run 20 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "3.4 min" 206
run 21 "(echo 'running 32'; cd 32/; sbatch -W slurm_start.sh)" "2.8 min" 166
run 22 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "4.9 min" 295
run 23 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "3.9 min" 231
run 24 "(echo 'running 01'; cd 01/; sbatch -W slurm_start.sh)" "3.7 min" 221
run 25 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "2.7 min" 160
run 26 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "3.0 min" 180
run 27 "(echo 'running 16'; cd 16/; sbatch -W slurm_start.sh)" "1.8 min" 109
run 28 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "1.5 min" 90
run 29 "(echo 'running 16'; cd 16/; sbatch -W slurm_start.sh)" "3.7 min" 221
