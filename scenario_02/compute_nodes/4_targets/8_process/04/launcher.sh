#!/usr/bin/env bash

        # ############################################################
        # This script was generated using hourglass 1.0.0
        # Date: 2021-11-24
        # Used Parameters:
        #./hourglass 'sbatch -W slurm_start.sh' 
        #	--r 10 --s 1.0 --e 10.0 
        #	--o 04/launcher.sh --u min
        #	--m False --v True
        # ############################################################

 
    run () {
        start_time="$(date -u +%s)"
        echo "Starting Execution ${1}...$(date)"
        ${2}
        end_time="$(date -u +%s)"
        elapsed="$(($end_time-$start_time))"
        echo "Execution ${1} finished at $(date). Elapsed time ${elapsed}"                
        echo "Sleep time for next execution:  ${3}"
        sleep "${4}"
        echo 
    }


run 0 'sbatch -W slurm_start.sh' '3.4 min' 201
run 1 'sbatch -W slurm_start.sh' '4.3 min' 259
run 2 'sbatch -W slurm_start.sh' '9.4 min' 564
run 3 'sbatch -W slurm_start.sh' '4.3 min' 256
run 4 'sbatch -W slurm_start.sh' '7.7 min' 459
run 5 'sbatch -W slurm_start.sh' '6.5 min' 391
run 6 'sbatch -W slurm_start.sh' '9.9 min' 593
run 7 'sbatch -W slurm_start.sh' '1.6 min' 96
run 8 'sbatch -W slurm_start.sh' '7.1 min' 426
run 9 'sbatch -W slurm_start.sh' '0.0 min' 0.0
