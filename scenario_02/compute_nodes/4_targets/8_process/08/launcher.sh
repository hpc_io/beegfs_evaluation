#!/usr/bin/env bash

        # ############################################################
        # This script was generated using hourglass 1.0.0
        # Date: 2021-11-24
        # Used Parameters:
        #./hourglass 'sbatch -W slurm_start.sh' 
        #	--r 10 --s 1.0 --e 10.0 
        #	--o 08/launcher.sh --u min
        #	--m False --v True
        # ############################################################

 
    run () {
        start_time="$(date -u +%s)"
        echo "Starting Execution ${1}...$(date)"
        ${2}
        end_time="$(date -u +%s)"
        elapsed="$(($end_time-$start_time))"
        echo "Execution ${1} finished at $(date). Elapsed time ${elapsed}"                
        echo "Sleep time for next execution:  ${3}"
        sleep "${4}"
        echo 
    }


run 0 'sbatch -W slurm_start.sh' '2.1 min' 124
run 1 'sbatch -W slurm_start.sh' '5.6 min' 336
run 2 'sbatch -W slurm_start.sh' '9.5 min' 569
run 3 'sbatch -W slurm_start.sh' '2.6 min' 157
run 4 'sbatch -W slurm_start.sh' '2.2 min' 132
run 5 'sbatch -W slurm_start.sh' '9.2 min' 551
run 6 'sbatch -W slurm_start.sh' '6.5 min' 392
run 7 'sbatch -W slurm_start.sh' '7.0 min' 418
run 8 'sbatch -W slurm_start.sh' '8.5 min' 512
run 9 'sbatch -W slurm_start.sh' '0.0 min' 0.0
