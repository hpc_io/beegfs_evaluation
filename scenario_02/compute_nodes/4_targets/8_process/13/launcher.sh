#!/usr/bin/env bash

        # ############################################################
        # This script was generated using hourglass 1.0.0
        # Date: 2021-11-24
        # Used Parameters:
        #./hourglass 'sbatch -W slurm_start.sh' 
        #	--r 10 --s 1.0 --e 10.0 
        #	--o r02/launcher.sh --u min
        #	--m False --v True
        # ############################################################

 
    run () {
        start_time="$(date -u +%s)"
        echo "Starting Execution ${1}...$(date)"
        ${2}
        end_time="$(date -u +%s)"
        elapsed="$(($end_time-$start_time))"
        echo "Execution ${1} finished at $(date). Elapsed time ${elapsed}"                
        echo "Sleep time for next execution:  ${3}"
        sleep "${4}"
        echo 
    }


run 0 'sbatch -W slurm_start.sh' '6.8 min' 406
run 1 'sbatch -W slurm_start.sh' '3.5 min' 208
run 2 'sbatch -W slurm_start.sh' '6.1 min' 365
run 3 'sbatch -W slurm_start.sh' '9.2 min' 549
run 4 'sbatch -W slurm_start.sh' '3.3 min' 198
run 5 'sbatch -W slurm_start.sh' '9.7 min' 582
run 6 'sbatch -W slurm_start.sh' '4.8 min' 285
run 7 'sbatch -W slurm_start.sh' '8.8 min' 526
run 8 'sbatch -W slurm_start.sh' '4.9 min' 296
run 9 'sbatch -W slurm_start.sh' '0.0 min' 0.0
