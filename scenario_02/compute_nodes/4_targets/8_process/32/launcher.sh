#!/usr/bin/env bash

        # ############################################################
        # This script was generated using hourglass 1.0.0
        # Date: 2021-10-05
        # Used Parameters:
        #./hourglass 'sbatch -W slurm_start.sh' 
        #	--r 1 --s 1.0 --e 5.0 
        #	--o /home/luan/Devel/luan-teylo/tests/omnipath/compute_nodes/8_targets/32/launcher.sh --u min
        #	--m False --v True
        # ############################################################

 
    run () {
        start_time="$(date -u +%s)"
        echo "Starting Execution ${1}...$(date)"
        ${2}
        end_time="$(date -u +%s)"
        elapsed="$(($end_time-$start_time))"
        echo "Execution ${1} finished at $(date). Elapsed time ${elapsed}"                
        echo "Sleep time for next execution:  ${3}"
        sleep "${4}"
        echo 
    }


run 0 'sbatch -W slurm_start.sh' '0.0 min' 0.0
