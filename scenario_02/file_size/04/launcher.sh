#!/usr/bin/env bash

        # ############################################################
        # This script was generated using hourglass 1.0.0
        # Date: 2021-11-22
        # Used Parameters:
        #./hourglass 'sbatch -W slurm_start.sh' 
        #	--r 10 --s 1.0 --e 10.0 
        #	--o 04/launcher.sh --u min
        #	--m False --v True
        # ############################################################

 
    run () {
        start_time="$(date -u +%s)"
        echo "Starting Execution ${1}...$(date)"
        ${2}
        end_time="$(date -u +%s)"
        elapsed="$(($end_time-$start_time))"
        echo "Execution ${1} finished at $(date). Elapsed time ${elapsed}"                
        echo "Sleep time for next execution:  ${3}"
        sleep "${4}"
        echo 
    }


run 0 'sbatch -W slurm_start.sh' '8.5 min' 511
run 1 'sbatch -W slurm_start.sh' '9.6 min' 575
run 2 'sbatch -W slurm_start.sh' '3.8 min' 230
run 3 'sbatch -W slurm_start.sh' '1.7 min' 103
run 4 'sbatch -W slurm_start.sh' '8.4 min' 507
run 5 'sbatch -W slurm_start.sh' '3.1 min' 185
run 6 'sbatch -W slurm_start.sh' '4.5 min' 273
run 7 'sbatch -W slurm_start.sh' '7.7 min' 463
run 8 'sbatch -W slurm_start.sh' '3.1 min' 184
run 9 'sbatch -W slurm_start.sh' '0.0 min' 0.0
