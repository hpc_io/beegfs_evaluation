#!/usr/bin/env bash

        # ############################################################
        # This script was generated using hourglass 1.0.0
        # Date: 2021-11-22
        # Used Parameters:
        #./hourglass 'sbatch -W slurm_start.sh' 
        #	--r 10 --s 1.0 --e 10.0 
        #	--o 08/launcher.sh --u min
        #	--m False --v True
        # ############################################################

 
    run () {
        start_time="$(date -u +%s)"
        echo "Starting Execution ${1}...$(date)"
        ${2}
        end_time="$(date -u +%s)"
        elapsed="$(($end_time-$start_time))"
        echo "Execution ${1} finished at $(date). Elapsed time ${elapsed}"                
        echo "Sleep time for next execution:  ${3}"
        sleep "${4}"
        echo 
    }


run 0 'sbatch -W slurm_start.sh' '2.9 min' 173
run 1 'sbatch -W slurm_start.sh' '8.3 min' 501
run 2 'sbatch -W slurm_start.sh' '8.8 min' 531
run 3 'sbatch -W slurm_start.sh' '6.0 min' 360
run 4 'sbatch -W slurm_start.sh' '5.9 min' 355
run 5 'sbatch -W slurm_start.sh' '1.6 min' 95
run 6 'sbatch -W slurm_start.sh' '5.2 min' 314
run 7 'sbatch -W slurm_start.sh' '9.0 min' 538
run 8 'sbatch -W slurm_start.sh' '1.5 min' 89
run 9 'sbatch -W slurm_start.sh' '0.0 min' 0.0
