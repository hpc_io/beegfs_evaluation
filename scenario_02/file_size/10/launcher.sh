#!/usr/bin/env bash

        # ############################################################
        # This script was generated using hourglass 1.0.0
        # Date: 2021-11-22
        # Used Parameters:
        #./hourglass 'sbatch -W slurm_start.sh' 
        #	--r 10 --s 1.0 --e 10.0 
        #	--o 10/launcher.sh --u min
        #	--m False --v True
        # ############################################################

 
    run () {
        start_time="$(date -u +%s)"
        echo "Starting Execution ${1}...$(date)"
        ${2}
        end_time="$(date -u +%s)"
        elapsed="$(($end_time-$start_time))"
        echo "Execution ${1} finished at $(date). Elapsed time ${elapsed}"                
        echo "Sleep time for next execution:  ${3}"
        sleep "${4}"
        echo 
    }


run 0 'sbatch -W slurm_start.sh' '3.0 min' 181
run 1 'sbatch -W slurm_start.sh' '3.2 min' 194
run 2 'sbatch -W slurm_start.sh' '3.0 min' 182
run 3 'sbatch -W slurm_start.sh' '9.3 min' 561
run 4 'sbatch -W slurm_start.sh' '6.4 min' 383
run 5 'sbatch -W slurm_start.sh' '10.0 min' 600
run 6 'sbatch -W slurm_start.sh' '5.4 min' 323
run 7 'sbatch -W slurm_start.sh' '4.0 min' 243
run 8 'sbatch -W slurm_start.sh' '3.7 min' 221
run 9 'sbatch -W slurm_start.sh' '0.0 min' 0.0
