#!/usr/bin/env bash

        # ############################################################
        # This script was generated using hourglass 1.0.0
        # Date: 2022-02-08
        # Used Parameters:
        #./hourglass  --cmd_file 'commands.txt' 
        #	--r 10 --s 1.0 --e 30.0 
        #	--o launcher.sh --u min
        #	--m False --v True
        # ############################################################

 
    run () {
        start_time="$(date -u +%s)"
        echo "Starting Execution ${1}...$(date)"
        eval ${2}
        end_time="$(date -u +%s)"
        elapsed="$(($end_time-$start_time))"
        echo "Execution ${1} finished at $(date). Elapsed time ${elapsed}"                
        echo "Sleep time for next execution:  ${3}"
        sleep "${4}"
        echo 
    }


run 0 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "19.2 min" 1150
run 1 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "11.8 min" 706
run 2 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "17.3 min" 1037
run 3 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "16.6 min" 993
run 4 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "11.2 min" 673
run 5 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "23.5 min" 1408
run 6 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "20.9 min" 1257
run 7 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "16.3 min" 977
run 8 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "14.0 min" 840
run 9 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "0.0 min" 0.0
run 10 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "12.6 min" 754
run 11 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "27.2 min" 1630
run 12 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "10.8 min" 647
run 13 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "25.6 min" 1538
run 14 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "25.5 min" 1530
run 15 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "14.5 min" 869
run 16 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "23.8 min" 1428
run 17 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "28.0 min" 1679
run 18 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "18.2 min" 1094
run 19 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "13.6 min" 815
run 20 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "24.1 min" 1447
run 21 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "21.9 min" 1312
run 22 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "8.2 min" 495
run 23 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "5.5 min" 333
run 24 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "17.9 min" 1077
run 25 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "26.7 min" 1603
run 26 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "14.2 min" 851
run 27 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "8.4 min" 504
run 28 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "9.7 min" 584
run 29 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "9.0 min" 538
