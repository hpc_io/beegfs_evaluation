#!/usr/bin/env bash
##########################
#That script executes IOR
##########################

#SBATCH --job-name=overlap_test_04

#####SBATCH -N 1 -n 8
#SBATCH --nodefile=nodes


# run for five minutes
#              d-hh:mm:ss
#SBATCH --time=0-5:00:00

##  standard output message
#SBATCH -o batch%j.out
# # output error message
#SBATCH -e batch%j.err
#SBATCH --exclusive -C bora
##

module purge
module load mpi/openmpi/4.0.3-mlx

echo "===== My JOB Information ===="
echo "Node List:  $SLURM_NODELIST"
echo "my jobID:  $SLURM_JOB_ID"
echo "Partition: $SLURM_JOB_PARTITION"
echo "submit directory:  $SLURM_SUBMIT_DIR"
echo "submit host:  $SLURM_SUBMIT_HOST"
echo "In the directory: $(pwd)"
echo "As the user: $(whoami)"
echo "============================"

export PATH=$PATH:/home/lgouveia/ior/bin
export PATH=$PATH:/home/lgouveia/luan-teylo/code

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/lgouveia/ior/lib

## PATH where IOR will write the file
IOR_PATH=/beegfs/lgouveia/ior_6

# Environment PER IOR Instance
n_nodes=8
n_tasks=64
n_tasks_per_node=8

# IOR PARAMETERS
parameters='-w -t 1m -b 512m -k'

# Number of executions
n=10
sleep_time=60 # time between consecutive executions

NODE_LIST_1='bora[003,004,005,007,008,009,010,011]'
NODE_LIST_2='bora[012,013,014,016,017,018,019,020]'
NODE_LIST_3='bora[022,023,024,025,026,027,028,029]'
NODE_LIST_4='bora[030,031,032,033,034,035,037,038]'


sigma=0
# Perform the execution block $n times.
x=1
while [ $x -le $n ]
do
  
  ## Aggregate Execution
  srun  --nodes="${n_nodes}" --ntasks="${n_tasks}" --ntasks-per-node="${n_tasks_per_node}"  --nodelist="${NODE_LIST_1}" ior ${parameters} -o ${IOR_PATH}/file_1  >> ${SLURM_JOB_ID}_A.out &
  srun  --nodes="${n_nodes}" --ntasks="${n_tasks}" --ntasks-per-node="${n_tasks_per_node}"  --nodelist="${NODE_LIST_2}" ior ${parameters} -o ${IOR_PATH}/file_2  >> ${SLURM_JOB_ID}_B.out &
  srun  --nodes="${n_nodes}" --ntasks="${n_tasks}" --ntasks-per-node="${n_tasks_per_node}"  --nodelist="${NODE_LIST_3}" ior ${parameters} -o ${IOR_PATH}/file_3  >> ${SLURM_JOB_ID}_C.out &
  srun  --nodes="${n_nodes}" --ntasks="${n_tasks}" --ntasks-per-node="${n_tasks_per_node}"  --nodelist="${NODE_LIST_4}" ior ${parameters} -o ${IOR_PATH}/file_4  >> ${SLURM_JOB_ID}_D.out &

  wait

  ## Call File Tracker
  file_tracker.py ${IOR_PATH} --d --v

  sleep ${sleep_time}
  x=$(( $x + 1 ))

done


