#!/usr/bin/env bash

        # ############################################################
        # This script was generated using hourglass 1.0.0
        # Date: 2022-02-09
        # Used Parameters:
        #./hourglass  --cmd_file 'commands.txt' 
        #	--r 10 --s 1.0 --e 10.0 
        #	--o launcher.sh --u min
        #	--m False --v True
        # ############################################################

 
    run () {
        start_time="$(date -u +%s)"
        echo "Starting Execution ${1}...$(date)"
        eval ${2}
        end_time="$(date -u +%s)"
        elapsed="$(($end_time-$start_time))"
        echo "Execution ${1} finished at $(date). Elapsed time ${elapsed}"                
        echo "Sleep time for next execution:  ${3}"
        sleep "${4}"
        echo 
    }


run 0 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "4.9 min" 294
run 1 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "7.0 min" 418
run 2 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "2.1 min" 124
run 3 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "7.3 min" 438
run 4 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "8.1 min" 488
run 5 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "9.1 min" 543
run 6 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "1.8 min" 105
run 7 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "2.8 min" 167
run 8 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "7.0 min" 421
run 9 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "0.0 min" 0.0
run 10 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "3.5 min" 210
run 11 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "6.0 min" 359
run 12 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "1.0 min" 61
run 13 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "4.7 min" 280
run 14 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "3.7 min" 222
run 15 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "7.2 min" 431
run 16 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "5.0 min" 302
run 17 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "1.2 min" 73
run 18 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "6.9 min" 416
run 19 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "7.2 min" 430
run 20 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "5.6 min" 336
run 21 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "8.9 min" 534
run 22 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "5.1 min" 308
run 23 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "3.7 min" 221
run 24 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "7.6 min" 457
run 25 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "5.5 min" 331
run 26 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "4.8 min" 287
run 27 "(echo 'running 08'; cd 08/; sbatch -W slurm_start.sh)" "7.0 min" 418
run 28 "(echo 'running 02'; cd 02/; sbatch -W slurm_start.sh)" "4.2 min" 252
run 29 "(echo 'running 04'; cd 04/; sbatch -W slurm_start.sh)" "5.4 min" 323
