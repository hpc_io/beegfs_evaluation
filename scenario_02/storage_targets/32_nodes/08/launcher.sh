#!/usr/bin/env bash

        # ############################################################
        # This script was generated using hourglass 1.0.0
        # Date: 2021-10-06
        # Used Parameters:
        #./hourglass 'sbatch -W slurm_start.sh' 
        #	--r 10 --s 1.0 --e 10.0 
        #	--o /home/luan/Devel/luan-teylo/tests/omnipath/storage_targets/08/launcher.sh --u min
        #	--m False --v True
        # ############################################################

 
    run () {
        start_time="$(date -u +%s)"
        echo "Starting Execution ${1}...$(date)"
        ${2}
        end_time="$(date -u +%s)"
        elapsed="$(($end_time-$start_time))"
        echo "Execution ${1} finished at $(date). Elapsed time ${elapsed}"                
        echo "Sleep time for next execution:  ${3}"
        sleep "${4}"
        echo 
    }


run 0 'sbatch -W slurm_start.sh' '4.1 min' 246
run 1 'sbatch -W slurm_start.sh' '9.5 min' 571
run 2 'sbatch -W slurm_start.sh' '9.7 min' 584
run 3 'sbatch -W slurm_start.sh' '4.5 min' 270
run 4 'sbatch -W slurm_start.sh' '4.3 min' 259
run 5 'sbatch -W slurm_start.sh' '4.9 min' 296
run 6 'sbatch -W slurm_start.sh' '6.5 min' 391
run 7 'sbatch -W slurm_start.sh' '3.8 min' 228
run 8 'sbatch -W slurm_start.sh' '7.6 min' 457
run 9 'sbatch -W slurm_start.sh' '0.0 min' 0.0
