subdir=raw_output/exec_01
outdir=csv/exec_01
test_dir=tests/omnipath/storage_targets



counter=1
while [ $counter -le 8 ]
do
   if [ $counter -lt "10" ]; then
     var=0$counter
  else
     var=$counter
  fi


	ior_2_csv.py ${test_dir}/${var}/ --get_files --subdir ${subdir} -o ${outdir}/output.csv --v

	mv $WORKDIR_LOCAL/${test_dir}/${var}/${subdir}/tracker.csv $WORKDIR_LOCAL/${test_dir}/${var}/${outdir}
	 counter=$(( ${counter} + 1 ))
done